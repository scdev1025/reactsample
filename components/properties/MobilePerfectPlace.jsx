import { Component } from "react";
import BuySelect from "./BuySelect";
import TitleSelect from "../shared/TitleSelect";
import styles from "./styles";

const propertyBg = "../static/images/home/img_property_bg.png";

class MobilePerfectPlace extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { notifyAction } = this.props;
    return (
      <div className="perfect-place">
        <div
          className="img-background blur-image"
          style={{ backgroundImage: `url(${propertyBg})` }}
        />
        <div className="heading white medium-large-size medium-font-weight text-center">
          Find the perfect place
        </div>
        <div className="buttons">
          <div className="flex-column-center">
            <div className="flex-row-between">
              <div className="buy-select-container">
                <BuySelect />
              </div>
              <div className="property-type-container">
                <TitleSelect title="Property Type" list="Any" />
              </div>
            </div>
            <div className="row no-gutters full-width">
              <TitleSelect
                title="Suburbs or postcodes"
                list="Elwood, Elsternwick, Port Melbourne"
              />
            </div>
            <div className="flex-row-between">
              <div className="col-md-6 p-1 m-0">
                <TitleSelect title="Min price" list="Any" />
              </div>
              <div className="col-md-6 p-1 m-0">
                <TitleSelect title="Max price" list="Any" />
              </div>
            </div>
            <div className="flex-row-between">
              <div className="col-md-6 p-1 m-0">
                <TitleSelect title="Min bedrooms" list="Three" />
              </div>
              <div className="col-md-6 p-1 m-0">
                <TitleSelect title="Min bathrooms" list="Any" />
              </div>
            </div>
            <div className="row full-width">
              <div className="col-md- p-1 m-0">
                <TitleSelect title="Min parking" list="Any" />
              </div>
            </div>
          </div>
          <div className="flex-column-center">
            <button
              type="button"
              className="btn btn-primary btn-search min-small-size shadow"
            >
              Search
            </button>
            <button
              type="button"
              className="btn btn-primary btn-notify min-small-size shadow"
              onClick={() => notifyAction(true)}
            >
              Notify me
            </button>
          </div>
        </div>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default MobilePerfectPlace;
