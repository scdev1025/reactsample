import css from "styled-jsx/css";

export default css`
  .perfect-place {
    height: 35vh;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }
  .perfect-place .heading {
    background: rgba(9, 0, 0, 0.4);
    padding: 10px 20px;
  }
  .perfect-place .btn-primary {
    margin: 4px;
    border: none;
    border-radius: 0;
    background: #ff8214;
    width: 90%;
    font-size: 1.5rem;
    padding: 1.15rem;
  }
  .perfect-place .btn-notify {
    background: #221e1f;
  }
  .perfect-place .buttons {
    margin-top: 20px;
  }

  .option-buttons {
    width: 80%;
  }
  .control-buttons {
    width: 20%;
  }

  .buy-select {
    background: white;
    min-width: 150px;
    width: 100%;
    padding: 9px 0px 9px 20px;
    -webkit-box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.31);
    -moz-box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.31);
    box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.21);
  }
  .buy-select select {
    color: #ff8124;
    background: transparent;
    border: none;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    width: 100%;
  }
  .buy-select select:focus {
    outline: none;
  }
  .buy-select span {
    position: absolute;
    margin-left: 20px;
    transform: translateX(20px);
  }
  .no-gutter.row {
    margin-left: 5px;
    margin-right: 5px;
  }

  .notifyme .title {
    font-size: 2rem;
  }
  .notifyme .content {
    color: #fff;
    font-weight: 500;
    margin-bottom: 30px;
  }
  .notifyme .content .content-text {
    margin-bottom: -20px;
  }
  .notifyme .join-form .form-control {
    border: #fff 1px solid;
    border-radius: 0;
    // font-size: 2rem;
    padding: 32px 10px !important;
    background-color: transparent;
    margin: 0px 10px 0px 0px;
    color: #fff;
  }
  .notifyme .join-form .btn {
    border: none;
    border-radius: 0;
    background: #ff8124;
    color: #fff;
    padding: 15px 20px;
  }

  .notifyme .cancel {
    font-size: 1.5rem;
    font-weight: 500;
    margin-top: 10px;
  }

  @media screen and (max-width: 767px) {
    .heading {
      width: 100%;
    }
    .perfect-place {
      height: 700px;
      display: flex;
      flex-direction: column;
      justify-content: flex-start;
      align-items: flex-start;
    }
    .perfect-place .img-background {
      height: 700px;
    }
    .perfect-place .btn-primary {
      height: 50px;
      font-size: 1.25rem;
      margin: 5px;
      width: 100%;
      padding: 0px;
    }
    .perfect-place .buttons {
      width: 100%;
      margin-top: 10px;
      padding: 0px 10px;
    }
    .perfect-place .buttons .flex-row-between {
      width: 100%;
    }
    .perfect-place .buttons .property-type-container {
      flex: 1;
      margin-left: 5px;
    }
    .buy-select {
      margin: 5px 0px;
      height: 70px;
    }
    .buy-select select {
      font-size: 2rem;
    }

    .notifyme .title {
      margin-top: 20px;
      font-size: 1.75rem;
    }
    .notifyme .content,
    .join-form {
      padding-left: 20px;
      padding-right: 20px;
      width: 80%;
    }
    .notifyme .join-form {
      padding: 0px;
    }
    .notifyme .form-inline .form-group {
      width: 100%;
    }
    .notifyme .join-form .form-control {
      border: #fff 1px solid;
      border-radius: 0;
      padding: 20px 10px !important;
      background-color: transparent;
      color: #fff;
      width: 100%;
      font-size: 1.25rem;
      font-weight: 300;
      text-align: center;
    }
    .notifyme .join-form .btn {
      width: 100%;
      padding: 5px;
      font-size: 1.25rem;
      font-weight: 300;
    }
    .notifyme .cancel {
      font-size: 1.25rem;
    }
  }
`;
