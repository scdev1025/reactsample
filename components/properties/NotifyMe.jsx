import { Component } from "react";
import styles from "./styles";

class NotifyMe extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { cancelAction } = this.props;

    return (
      <div className="notifyme flex-column-center text-center">
        <div className="orange title">
          <p>BE THE FIRST TO KNOW</p>
        </div>
        <div className="join-form">
          <form className="form-inline">
            <div className="form-group ">
              <input
                type="username"
                className="form-control small-size"
                id="username"
                placeholder="Your name"
              />
            </div>
            <div className="form-group ">
              <input
                type="useremail"
                className="form-control small-size"
                id="useremail"
                placeholder="Your email"
              />
            </div>
            <button type="submit" className="btn btn-primary small-size">
              Join the club
            </button>
          </form>
        </div>
        <a href="#" className="orange cancel" onClick={() => cancelAction(false)}>
          Cancel
        </a>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default NotifyMe;
