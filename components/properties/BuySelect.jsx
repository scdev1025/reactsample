import { Component } from "react";
import styles from "./styles";

class BuySelect extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="buy-select flex-column-center">
        <select className="text-center medium-large-size">
          <option>Buy</option>
          <option>Buy</option>
          <option>Buy</option>
        </select>
        <span className="fas fa-sort-down orange small-size" />
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default BuySelect;
