import { Component } from "react";
import styles from "./styles";
import { tickIcon, arrowDownIcon } from "../../shared/variables";
import ReadMoreButton from "../../shared/ReadMoreButton";
import MapViewItem from "../../shared/MapViewItem";
const imgMapView = "../../../static/images/properties/img_map_view.png";

class MainContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mapViewIndex: 0
    };
  }

  onPressMapView(viewIndex) {
    this.setState({
      mapViewIndex: viewIndex
    });
  }

  render() {
    const { property } = this.props;
    const { mapViewIndex } = this.state;

    return (
      <div className="main-content">
        <div className="heading">{property.subtitle}</div>
        <div className="content">
          <p>
            With the soul of an elegent Edwardian and the pizzazz of an Art Deco
            Diva, this fabous freestanding four-bedroom family home is situated
            in a quiet tree-lined street in the heart of Elwood. Boasting a
            large backyard with a northerly rear aspect and just an easy stroll
            from a choice of shopping strips, cafes & transport options, there
            is everything you could possibly need right on hand.
          </p>
          <div className="readmore">
            <ReadMoreButton />
          </div>
          <p className="more-content">
            The curvaceous waterfall facade added during the fifties is
            complemented by more recent enhancements which have resulted in a
            truly stunning abode. Characterised by high ceilings, rich ebony
            timber floors and spacious rooms, the home offers a flexible
            floorplan and multiple living zones. The curvaceous waterfall facade
            added during the fifties is complemented by more recent enhancements
            which have resulted in a truly stunning abode. Characterised by high
            ceilings, rich ebony timber floors and spacious rooms, the home
            offers a flexible floorplan and multiple living zones. The
            curvaceous waterfall facade added during the fifties is complemented
            by more recent enhancements which have resulted in a truly stunning
            abode. Characterised by high ceilings, rich ebony timber floors and
            spacious rooms, the home offers a flexible floorplan and multiple
            living zones.
          </p>
          <p className="more-content">
            With the soul of an elegent Edwardian and the pizzazz of an Art Deco
            Diva, this fabous freestanding four-bedroom family home is situated
            in a quiet tree-lined street in the heart of Elwood. Boasting a
            large backyard with a northerly rear aspect and just an easy stroll
            from a choice of shopping strips, cafes & transport options, there
            is everything you could possibly need right on hand.
          </p>
        </div>
        <div className="property-features">
          <div className="heading">PROPERTY FEATURES</div>
          <div className="feature-list row">
            <ul className="col-md-4">
              {property.featureList.map((item, index) => (
                <li key={index}>
                  <img src={tickIcon} />
                  <span className="feature-item">{item}</span>
                </li>
              ))}
            </ul>
            <ul className="col-md-4 more-content">
              {property.featureList.map((item, index) => (
                <li key={index}>
                  <img src={tickIcon} />
                  <span className="feature-item">{item}</span>
                </li>
              ))}
            </ul>
            <ul className="col-md-4 more-content">
              {property.featureList.map((item, index) => (
                <li key={index}>
                  <img src={tickIcon} />
                  <span className="feature-item">{item}</span>
                </li>
              ))}
            </ul>
          </div>
        </div>
        <div className="button-container">
          <button type="button" className="btn btn-primary btn-checkout shadow">
            Check out the floorplan
          </button>
          <button type="button" className="btn btn-primary shadow">
            View statement of information
          </button>
        </div>
        <div className="view-section">
          <div
            className="img-background"
            style={{ backgroundImage: `url(${imgMapView})` }}
          >
            <div className="btn-group" role="group">
              <div className="view-button">
                <button
                  type="button"
                  className={mapViewIndex == 0 ? "btn selected" : "btn"}
                  onClick={() => this.onPressMapView(0)}
                >
                  Map view
                </button>
                <img
                  src={arrowDownIcon}
                  className={mapViewIndex == 0 ? "img-selected" : ""}
                />
              </div>
              <div className="view-button">
                <button
                  type="button"
                  className={mapViewIndex == 1 ? "btn selected" : "btn"}
                  onClick={() => this.onPressMapView(1)}
                >
                  Street View
                </button>
                <img
                  src={arrowDownIcon}
                  className={mapViewIndex == 1 ? "img-selected" : ""}
                />
              </div>
              <div className="view-button">
                <button
                  type="button"
                  className={mapViewIndex == 2 ? "btn selected" : "btn"}
                  onClick={() => this.onPressMapView(2)}
                >
                  Local Schools
                </button>
                <img
                  src={arrowDownIcon}
                  className={mapViewIndex == 2 ? "img-selected" : ""}
                />
              </div>
              <div className="home-item">
                <MapViewItem title={"23 Baker St, St Kilda"} isHome={true}/>
              </div>
              <div className="school-item">
                <MapViewItem title={"St Kilda Primary School"} isHome={false}/>
              </div>
            </div>
            <button
              type="button"
              className="btn btn-primary btn-direction shadow"
            >
              Get directions
            </button>
          </div>
        </div>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default MainContent;
