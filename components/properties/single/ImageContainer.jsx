import styles from "./styles";
import ImageSlider from "../../shared/ImageSlider";
import VerticalImageSlider from "../../shared/VerticalImageSlider";

const ImageContainer = props => {
  return (
    <div className="image-container">
      <div className="horizontal-slider">
        <ImageSlider imageList={props.imageList} />
      </div>
      <div className="vertical-slider">
        <VerticalImageSlider imageList={props.imageList} />
      </div>
      <style jsx>{styles}</style>
    </div>
  );
};

export default ImageContainer;
