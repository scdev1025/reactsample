import { Component } from "react";
import styles from "./styles";
import { calendarIcon } from "../../shared/variables";
import PeopleItem from "../../shared/PeopleItem";

class AdditionalContent extends Component {
  render() {
    const { property } = this.props;

    return (
      <div className="additional-content">
        <div className="button-container">
          <button type="button" className="btn btn-primary btn-checkout shadow">
            Check out the floorplan
          </button>
          <button type="button" className="btn btn-primary shadow">
            View statement of information
          </button>
        </div>
        <div className="add-auction">
          <div className="price">
            <div className="heading">PRICE:</div> {property.price}
          </div>
          <div className="auction">
            <div className="heading">AUCTION:</div> {property.auction}
          </div>
          <button type="button" className="btn btn-primary btn-auction shadow">
            Add auction to calendar
          </button>
        </div>
        <div className="inspections">
          <div className="heading">INSPECTIONS</div>
          <ul>
            {property.inspectionList.map((item, index) => (
              <li key={index}>
                <img src={calendarIcon} />
                <span className="inspection-time">{item}</span>
              </li>
            ))}
          </ul>
        </div>
        <div className="people-list">
          <PeopleItem />
          <PeopleItem />
        </div>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default AdditionalContent;
