import { Component } from "react";
import styles from "./styles";
import { bedIcon, bathIcon, carIcon } from "../../shared/variables";

class TopBar extends Component {
  render() {
    const { property, contactAgentAction, isContactAgent } = this.props;

    return (
      <div className="top-bar">
        <div className="center-title">
          {property.address + ", " + property.title}
        </div>
        <div className="left-content">
          <div className="property-icon">
            <ul>
              <li className="shadow">
                <a href="#">
                  <img src={bedIcon} alt="icon_bed" />
                  <span className="count">{property.bed_count}</span>
                </a>
              </li>
              <li className="shadow">
                <a href="#">
                  <img src={bathIcon} alt="icon_bath" />
                  <span className="count">{property.bath_count}</span>
                </a>
              </li>
              <li className="shadow">
                <a href="#">
                  <img src={carIcon} alt="icon_car" />
                  <span className="count">{property.car_count}</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div className="right-content">
          <a className="btn btn-default" onClick={contactAgentAction}>
            {isContactAgent ? "Cancel" : "Contact agent"}
          </a>
        </div>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default TopBar;
