import css from "styled-jsx/css";

export default css`
  .top-bar {
    padding-top: 100px;
    background: #ff8124;
    -webkit-box-shadow: 0px 9px 9px -5px rgba(0, 0, 0, 0.31);
    -moz-box-shadow: 0px 9px 9px -5px rgba(0, 0, 0, 0.31);
    box-shadow: 5px 9px 9px -5px rgba(0, 0, 0, 0.31);
    position: relative;
    z-index: 1;    
    display: flex;    
    padding-left: 20px;
    padding-right: 20px;
    justify-content: space-between;
    align-items: center; 
    height: 170px;
  }    
  .top-bar .center-title{    
    position: absolute;
    left: 0;
    color: white;    
    font-size: 2rem;
    font-weight: 500;
    text-align: center;
    width: 100%;
  }
  .top-bar .left-content .property-icon {
    width: auto;
  }
  .top-bar .right-content {
    z-index: 1;
  }
  .top-bar .right-content .btn {
    -webkit-box-shadow: 2px 2px 5px 2px rgba(0, 0, 0, 0.21);
    -moz-box-shadow: 2px 2px 5px 2px rgba(0, 0, 0, 0.21);
    box-shadow: 2px 2px 5px 2px rgba(0, 0, 0, 0.21);
    padding: 10px 27px;
    background: #fff;
    color: black;
    font-size: 1.25rem;
    border-radius: 0;    
    margin-bottom: 0px;     
  }

  .image-container {
    background: white;  
    height: 80vh;
    display: flex;
    flex-direction: row;
  }
  .image-container .horizontal-slider {
    width: 75%;
    height: 100%:    
    overflow: hidden;
  }
  .image-container .vertical-slider {    
    width: 25%;
    overflow: hidden;     
  }

  .main-content {
    padding: 10px;
  }
  .main-content .heading{
    font-size: 2rem;
    font-weight: 500;
    color: #ff8124;
    margin: 30px 0px;
  }
  .main-content .content{
    font-size: 1.25rem;    
  }
  .main-content .content .readmore{
    display: none;
  }
  .main-content .button-container{
    display: none;
  }

  .property-features .heading {
    font-size: 1.5rem;
    margin-top: 50px;
    margin-bottom: 30px;
  }
  .feature-list {
    padding: 5px;
  }
  .feature-list .feature-item {
    font-size: 1.25rem;
    margin-left: 10px;
  }
  .feature-list li {
    display: flex;
    align-items: center;    
  }
  .view-section {
    margin-top: 50px;
    height: 600px;
    margin-left: -25px;
    margin-right: -25px;
    overflow: hidden;  
  }
  .view-section .img-background {
    height: 100%;    
    display: flex;
    flex-direction: column;
    justify-content: space-between;
  }
  .view-section .img-background .home-item {    
    position: relative;
    diplay: inline-block;    
    transform: translate(10vw, 10vh);
  }
  .view-section .img-background .school-item {    
    position: relative;
    diplay: inline-block;    
    transform: translate(20vw, 20vh);
  }
  .view-section .btn-group {
    background: white;
    border: 1px solid #ff8214;
    height: 44px;
  }
  .view-section .btn-group .view-button {
    display: inline-block;
  }
  .view-section .btn-group .btn {
    border-left: 1px solid #ff8214;
    border-right: 1px solid #ff8214;
    background: white;
    border-radius: 0px;
    height: 100%;
    font-size: 1.25rem;    
    color: #ff8214;
    width: 200px;
  }
  .view-section .btn-group .selected {
    color: white;
    background: #ff8214;
  }  
  .view-section .btn-group img {
    position: absolute;    
    transform: translate(-115px, 38px);
    display: none;
  }
  .view-section .btn-group img.img-selected {
    display: inline;
  }

  .view-section .btn-direction {  
    background: #ff8124;
    border: none;
    border-radius: 0;
    font-size: 1.25rem;
    padding: 5px 10px;
    margin-left: 20px;
    margin-bottom: 20px;
    width: 200px;
  }  

  .additional-content {
    padding: 10px;
    padding-top: 50px;
    display: flex;
    flex-direction: column;
    background: #EFEFEF;
  }
  .btn {
    font-size: 1.25rem;    
    background: #221e1f;
    border: none;
  }
  .button-container .btn, addition-content .btn {
    margin-bottom: 20px;
  }
  .button-container {
    display: flex;
    flex-direction: column;
  }
  .button-container .btn-checkout {
    background: #ff8214;
  }
  
  .additional-content .add-auction {
    display: flex;
    flex-direction: column;
    margin-top: 20px;
    font-size: 1.5rem;
  }
  .additional-content .add-auction .price, .additional-content .add-auction .auction{
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: flex-start;
  }
  .additional-content .add-auction .heading{
    color: #ff8214; 
    font-weight: 500;   
    margin-right: 10px;
  }
  .additional-content .add-auction .btn-auction {
    margin-top: 10px;
  }
  .additional-content .inspections .heading{
    margin: 20px 0px;
    font-size: 1.5rem;
    color: #ff8214;
    font-weight: 500;
  }
  .additional-content .inspections .inspection-time{    
    font-size: 1.25rem;    
    margin-left: 10px;
  }
  .inspections li {
    display: flex;
    align-items: center;
    margin-bottom: 5px;
  }

  .additional-content .people-list{
    display: flex;
    flex-direction: column;
    margin-top: 50px;
  }

  @media screen and (max-width: 767px) {
    .top-bar {
      padding: 10px 10px;
      display: flex;    
      flex-direction: column;
      justify-content: center;
      align-items: center; 
    }  
    .top-bar .center-title{    
      position: relative;            
      font-size: 1.5rem;        
    }
    .top-bar .left-content {
      height: 70px;      
      display: flex;
      flex-direction: row;
      justify-content: center;
      align-items: center; 
      margin-left: 0px;      
    }
    .top-bar .left-content .property-icon {
      display: flex;
      flex-direction: row;
      justify-content: center;
      align-items: center; 
      transform: translateY(0%);
    }
    .top-bar .right-content {
      padding: 10px;      
    }
    .top-bar .right-content .btn {                  
      width: 250px;
      font-size: 1.25rem;      
    }

    .image-container {   
      width: 100%;   
      height: 400px;
      flex-direction: column;
    }
    .image-container .horizontal-slider {
      height: 75%;
      width: 100%;      
    }
    .image-container .vertical-slider {    
      height: 25%;
      width: 100%;  
    }
  
    .main-content {
      padding: 10px 25px 0px 25px;
    }
    .main-content .heading{
      font-size: 1.75rem;      
    }
    .main-content .content{
      font-size: 1.25rem;    
    }
    .main-content .content .readmore{
      display: inherit;
    }
    .more-content{
      display: none;
    }
    .property-features .heading {
      font-size: 1.75rem;
      margin-top: 30px;
      margin-bottom: 0px;
    }
    .feature-list .feature-item {
      font-size: 1.25rem;
    }
    .main-content .button-container {
      background: #EFEFEF;
      margin: 10px -25px 0px -25px;      
      padding: 25px 25px 0px 25px;
      display: inherit;
    }
    .main-content .button-container .btn {
      font-size: 1.25rem;    
      font-weight:300;  
      width: 100%;
    }
    .view-section{
      margin-top: 0px;               
    }
    .view-section .img-background {
      display: flex;
      flex-direction: column;  
      align-items: center;
      justify-content: space-between;      
    }
    .view-section .btn-direction {        
      font-size: 1.25rem;
      margin-left: 0px;            
    }
    .view-section .btn-group {
      background: transparent;
      border: 0px solid white;
      height: auto;
      display: flex;
      flex-direction: column;
      align-items: flex-end;
      width: 100%;
    }
    .view-section .btn-group .btn {      
      border: 1px solid #ff8214; 
      background: white;
      border-radius: 0px;
      height: 100%;
      font-size: 1.25rem;
      padding: 0px;  
      height: 50px;   
      margin-bottom: 0px;       
    }
    .view-section .btn-group .selected {
      color: white;
      background: #ff8214;
    }  
    .view-section .btn-group img {
      position: absolute;    
      transform: translate(-220px, 17px) rotate(90deg);  
      display: none;
    }
    .view-section .btn-group img.img-selected {
      display: inline;
    }

    .additional-content {
      padding:20px 20px;
    }
    .additional-content .people-list{
      margin-left: -20px;
      margin-right: -20px;
    }
    .additional-content .button-container {
      display: none;
    }
    .additional-content .btn {
      font-size: 1.25rem;    
      font-weight:300;  
    }
    .additional-content .add-auction {
      font-size: 1.5rem;      
    }
    .additional-content .add-auction .heading {
      font-size: 1.75rem;      
    }
    .additional-content .add-auction .price, .additional-content .add-auction .auction{
      display: flex;
      flex-direction: column;
      align-items: flex-start;      
    }
  }
`;
