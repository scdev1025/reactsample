import { Component } from "react";
import { Link } from "../../routes";
import {
  sellIcon,
  buyIcon,
  leaseIcon,
  rentIcon,
  moreIcon
} from "../shared/variables";

const menuTitles = ["Sell", "Buy", "Lease", "Rent", "More"];

const menuIcons = [sellIcon, buyIcon, leaseIcon, rentIcon, moreIcon];

class MobileMenu extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="mobile-menu">
        {menuIcons.map((icon, index) => (
          <Link href="/">
            <a key={index} className="menu-item">
              <img src={icon} alt={icon} />
              <span className="menu-title">{menuTitles[index]}</span>
            </a>
          </Link>
        ))}
      </div>
    );
  }
}

export default MobileMenu;
