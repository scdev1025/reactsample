import { Component } from "react";
import { whiteLogo } from "../shared/variables";
import {
  facebookIcon,
  twitterIcon,
  instagramIcon,
  pinterestIcon,
  blogIcon
} from "../shared/variables";
import PropTypes from "prop-types";

const links = {
  title: "Quick links",
  linkList1: [
    "Company",
    "Our Team",
    "Careers at C&G",
    "Property Management",
    "Finance",
    "Blog",
    "Terms & Conditions",
    "Privacy Policy"
  ],
  linkList2: [
    "Buying",
    "Selling",
    "Renting",
    "Sales Inspections",
    "Rental Inspections",
    "New Developments"
  ]
};

const socialIcons = [
  facebookIcon,
  twitterIcon,
  instagramIcon,
  pinterestIcon,
  blogIcon
];

class Footer extends Component {
  render() {
    const {locationList} = this.props;

    return (
      <footer>
        <section className="footer-top">
          <div className="container-fluid">
            <div className="row no-gutters">
              <div className="quicklinks-container">
                <div className="quicklinks">
                  <h3>{links.title}</h3>
                  <ul className="col-md-6">
                    {links.linkList1.map((item, index) => (
                      <li key={index}>
                        <a href="#">{item}</a>
                      </li>
                    ))}
                  </ul>
                  <ul className="col-md-6">
                    {links.linkList2.map((item, index) => (
                      <li key={index}>
                        <a href="#">{item}</a>
                      </li>
                    ))}
                  </ul>
                </div>
              </div>
              <div className="ourlocation-container">
                <h3>Our locations</h3>
                <ul className="ourlocation">
                  {locationList.map((item, index) => (
                    <li key={index}>
                      <a href="#">
                        <div className="content-title orange">
                          {item.title}
                        </div>
                        <div className="white min-small-size">
                          {item.address}
                        </div>
                        <i className="fas fa-arrow-right" />
                      </a>
                    </li>
                  ))}
                </ul>
              </div>
              <div className="feellike-container">
                <h3>Feel like a local</h3>
                <div className="feellike-block">
                  <div className="heading">
                    {" "}
                    All the latest bayside property and real estate news direct
                    from our team to your inbox...
                  </div>
                  <form className="form-inline">
                    <div className="form-group ">
                      <input
                        type="yourmail"
                        className="form-control"
                        id="youreamil"
                        placeholder="Your Email"
                      />
                    </div>
                    <button type="submit" className="btn btn-primary">
                      Sign me up
                    </button>
                  </form>

                  <div className="footer-last-block-content">
                    <p>
                      Data supplied by <span>AgentBox</span>
                    </p>
                    <p>
                      Designed and built by <span>TechEquipt</span>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="footer-bottom">
          <div className="container-fluid">
            <div className="row no-gutters">
              <div className="col-md-5">
                <a href="#" className="cg">
                  <img src={whiteLogo} alt="img_logo_white" />
                </a>
              </div>
              <div className="col-md-7">
                <ul className="float-right">
                  {socialIcons.map((icon, index) => (
                    <li key={index}>
                      <a href="#">
                        <img src={icon} alt={icon} />
                      </a>
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          </div>
        </section>
        <section className="footer-bottom-link">
          <a href="#">Terms & Conditions</a>
          &nbsp;&nbsp;|&nbsp;&nbsp;
          <a href="#">Privacy Policy</a>
        </section>
      </footer>
    );
  }
}

Footer.defaultProps = {
  locationList: [
    {
      title: "Elwood",
      address: "90 Ormond Road",
      phone: "03 9531 1245",
      image: "./static/images/offices/img_office_elwood.png"
    },
    {
      title: "Port Melboume",
      address: "1/103D Bay Street",
      phone: "03 9646 4444",
      image: "./static/images/offices/img_office_portmelboume.png"
    },
    {
      title: "Black Rock",
      address: "3 Bluff Road",
      phone: "03 9589 3133",
      image: "./static/images/offices/img_office_blackrock.png"
    },
    {
      title: "Mount Martha",
      address: "7A Bay Road",
      phone: "03 9589 3133",
      image: "./static/images/offices/img_office_blackrock.png"
    }
  ]
};

Footer.propTypes = {
  locationList: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      address: PropTypes.string.isRequired,
      phone: PropTypes.string.isRequired,
      image: PropTypes.string.isRequired
    })
  ).isRequired
};

export default Footer;
