import { Component } from "react";
import {Link} from '../../routes';
import { LogoIcon } from "../shared/variables";

const menuTitles = ["Sell", "Buy me", "Lease", "Rent me"];

class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isTop: true
    };
    this.onScroll = this.onScroll.bind(this);
  }

  componentDidMount() {
    document.addEventListener("scroll", () => {
      const isTop = window.scrollY < 300;
      if (isTop !== this.state.isTop) {
        this.onScroll(isTop);
      }
    });
  }

  onScroll(isTop) {
    this.setState({ isTop });
  }

  render() {
    return (
      <header
        className={
          this.state.isTop
            ? "container-fluid shadow"
            : "shrink container-fluid shadow"
        }
      >
        <div className="row no-gutters">
          <div className="col-md-4">
            <Link href="/">
              <a className="logo">
                <img className="brand-logo" src={LogoIcon} alt="logo" />
              </a>
            </Link>
          </div>
          <div className="col-md-8 header-right-links">
            <ul className="list-inline float-right">
              {menuTitles.map(menuTitle => (
                <li key={menuTitle}>
                  <a href="javascript:void(0)">
                    {menuTitle}
                    <b>.</b>
                  </a>
                </li>
              ))}
              <li>
                <a href="javascript:void(0)">
                  More
                  <b>…</b>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </header>
    );
  }
}

export default Header;
