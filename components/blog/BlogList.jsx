import BlogItem from "./BlogItem";
import styles from "./styles";

const BlogList = props => {
  return (
    <div className="blog-list">
      {props.blogList.map((blog, index) => (
        <BlogItem key={index} blog={blog} />
      ))}
      <style jsx>{styles}</style>
    </div>
  );
};

export default BlogList;
