import { Component } from "react";
import styles from "./styles";

class BlogItem extends Component {
  render() {
    const { blog } = this.props;

    return (
      <div
        className="blog-item img-background"
        style={{ backgroundImage: `url(${blog.imageURL})` }}
      >
        <div className="blog-container">
          <div className="row">
            <div className="blog-title white col-md-6">{blog.title}</div>
          </div>
          <div className="row">
            <div className="blog-content white col-md-6">{blog.content}</div>
          </div>
          <div className="blog-category orange">{blog.category}</div>
          <div className="blog-date white">{blog.date}</div>
        </div>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default BlogItem;
