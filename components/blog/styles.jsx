import css from "styled-jsx/css";

export default css`
  .blog {
    height: 500px;
  }
  .blog-container {
    height: 100%;
    background: rgba(0, 0, 0, 0.5);
    padding-left: 20px;
    padding-bottom: 20px;
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
  }
  .blog-container .blog-title {
    font-size: 2.5rem;
    font-weight: 500;
    line-height: 100%;
    margin-bottom: 10px;
  }
  .blog-container .blog-content {
    font-size: 1rem;
  }
  .blog-container .blog-category {
    font-size: 2rem;
    font-weight: 500;
  }
  .blog-container .blog-date {
    font-size: 1.25rem;
    font-weight: 500;
  }

  .blog-item {
    height: 350px;
    margin-bottom: 20px;
    -webkit-box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.31);
    -moz-box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.31);
    box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.21);
  }
  .blog-list {
    margin-bottom: 40px;
  }

  .search-container .buttons {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
  }
  .btn-search {
    border: none;
    border-radius: 0;
    background: #ff8124;
    color: #fff;
    width: 90%;
    padding: 1.2rem;
    font-size: 1.5rem;
  }
  .sortby-container {
    display: flex;
    justify-content: flex-end;
  }

  .signup {
    background: white;
    padding: 30px;
  }
  .signup .heading {
    font-size: 1.25rem;
    font-weight: 500;
  }
  .signup .btn-primary {
    border: none;
    border-radius: 0;
    background: #ff8214;
    width: 100%;
    font-size: 1.25rem;
    padding: 0.8rem 1.3125rem;
    -webkit-box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.31);
    -moz-box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.31);
    box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.21);
  }

  @media screen and (max-width: 767px) {
    .blog-item {
      height: 500px;
    }

    .blog-container .blog-title {
      font-size: 2.25rem;
    }
    .blog-container .blog-date {
      font-size: 1.75rem;
    }

    .search-container .buttons {
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
    }

    .btn-search {
      padding: 0.75rem;
    }
  }
`;
