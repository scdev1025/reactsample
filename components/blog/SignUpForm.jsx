import { Component } from "react";
import StyledInput from "../shared/StyledInput";
import styles from "./styles";

class SignUpForm extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <form action="" className="signup">
        <div className="form-group">
          <div className="row clearfix">
            <div className="heading col-md-12">
              Subscribe to the C&G Blog! Stay up to date with everything
              Bayside, from property to restaurants, community events and more.
              We promise not to use your email for any other purpose.
            </div>
          </div>
        </div>
        <div className="form-group">
          <div className="row clearfix">
            <div className="col-md-12">
              <StyledInput title="Name" placeholder="" />
            </div>
          </div>
        </div>
        <div className="form-group">
          <div className="row clearfix">
            <div className="col-md-12">
              <StyledInput title="Email" placeholder="" />
            </div>
          </div>
        </div>

        <button type="submit" className="btn btn-primary" title="Submit">
          Sign me up
        </button>
        <style jsx>{styles}</style>
      </form>
    );
  }
}

export default SignUpForm;
