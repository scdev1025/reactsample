import { Component } from "react";
import { Carousel, CarouselItem, CarouselControl } from "reactstrap";
import styles from "./styles";

class BlogCarousel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeIndex: 0,
    };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
  }
  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next() {
    if (this.animating) return;
    const nextIndex =
      this.state.activeIndex === this.props.blogList.length - 1
        ? 0
        : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous() {
    if (this.animating) return;
    const prevIndex =
      this.state.activeIndex === 0
        ? this.props.blogList.length - 1
        : this.state.activeIndex - 1;
    this.setState({ activeIndex: prevIndex });
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  }
  render() {
    const { activeIndex } = this.state;
    const { blogList } = this.props;

    const slides = blogList.map(item => {
      return (
        <CarouselItem
          onExiting={this.onExiting}
          onExited={this.onExited}
          key={item.title}
        >
          <div
            className="blog img-background"
            style={{ backgroundImage: `url(${item.imageURL})` }}
          >
            <div className="blog-container">
              <div className="row">
                <div className="blog-title white col-md-6">
                  {item.title}
                </div>
              </div>
              <div className="blog-category orange">{item.category}</div>
              <div className="blog-date white">{item.date}</div>
            </div>
            <style jsx>{styles}</style>
          </div>
        </CarouselItem>
      );
    });

    return (
      <Carousel
        activeIndex={activeIndex}
        next={this.next}
        previous={this.previous}
      >
        {/* <CarouselIndicators items={this.state.saleItems} activeIndex={activeIndex} onClickHandler={this.goToIndex} /> */}
        {slides}
        <CarouselControl
          direction="prev"
          directionText="Previous"
          onClickHandler={this.previous}
        />
        <CarouselControl
          direction="next"
          directionText="Next"
          onClickHandler={this.next}
        />
      </Carousel>
    );
  }
}

export default BlogCarousel;
