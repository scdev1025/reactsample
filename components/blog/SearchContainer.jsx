import { Component } from "react";
import TitleSelect from "../shared/TitleSelect";
import SortBySelect from "../shared/SortBySelect";
import styles from "./styles";

class SearchContainer extends Component {
  render() {
    return (
      <div className="search-container">
        <div className="buttons flex-row-between">
          <div className="flex-row-between col-md-9">
            <div className="col-md-4 p-1 m-1">
              <TitleSelect title="Category" list="Homes" />
            </div>
            <div className="col-md-8 p-1 m-1">
              <TitleSelect title="Find something" list="Nam" />
            </div>
          </div>
          <div className="col-md-3 p-1 m-2 flex-row-center">
            <button type="button" className="btn btn-primary btn-search shadow">
              Search
            </button>
          </div>
        </div>
        <div className="sortby-container col-md-8">
          <SortBySelect />
        </div>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default SearchContainer;
