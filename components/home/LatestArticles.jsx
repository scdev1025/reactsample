import { Component } from "react";
import styles from "./styles";

class LatestArticles extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { articleList } = this.props;

    return (
      <div className="articles-container flex-column-center">
        <div className="black title text-center">
          <p>OUR LATEST ARTICLES</p>
        </div>
        <div className="list">
          <ul>
            {articleList.map((item, index) => (
              <li key={index} className="gray-border">
                <a href="#">
                  <div className="content-title orange">{item.title}</div>
                  <div className="article-content black min-small-size">
                    {item.content}
                  </div>
                  <div className="black min-small-size text-content font-italic">
                    <strong>
                      by <span className="orange">{item.author}</span> on{" "}
                      {item.date}
                    </strong>
                  </div>
                </a>
              </li>
            ))}
          </ul>
        </div>
        <div className="article-btn-container">
          <button type="button" className="btn btn-primary small-size">
            See all our articles
          </button>
        </div>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default LatestArticles;
