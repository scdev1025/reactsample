import { Component } from "react";
import styles from "./styles";
import { bedIcon, bathIcon, carIcon } from "../shared/variables";

class MobileFeaturedListings extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { salesProperty, rentalProperty } = this.props;

    return (
      <div className="intro-property">
        <div className="title-container flex-column-center">
          <div className="orange title small-medium-size text-center">
            <div>CREAM OF THE CROP</div>
          </div>
          <div className="">
            <div className="black small-size text-center">
              Our best properties of the week
            </div>
          </div>
        </div>
        <div className="property">
          <div
            className="property-img flex-column-center img-background"
            style={{ backgroundImage: `url(${salesProperty.imageURL})` }}
          >
            <div className="white property-desc text-center small-size medium-font-weight">
              Sales property of the week
            </div>
          </div>
          <div className="property-icon flex-column-center">
            <ul>
              <li className="shadow">
                <a href="#">
                  <img src={bedIcon} alt="icon_bed" />
                  <span className="count">{salesProperty.bed_count}</span>
                </a>
              </li>
              <li className="shadow">
                <a href="#">
                  <img src={bathIcon} alt="icon_bath" />
                  <span className="count">{salesProperty.bath_count}</span>
                </a>
              </li>
              <li className="shadow">
                <a href="#">
                  <img src={carIcon} alt="icon_car" />
                  <span className="count">{salesProperty.car_count}</span>
                </a>
              </li>
            </ul>
          </div>
          <div className="property-title-container flex-column-center">
            <span className="property-title orange large-size medium-font-weight">
              {salesProperty.title}
            </span>
          </div>
        </div>
        <div className="property">
          <div
            className="property-img flex-column-center img-background"
            style={{ backgroundImage: `url(${rentalProperty.imageURL})` }}
          >
            <div className="white property-desc text-center small-size medium-font-weight">
              Rental property of the week
            </div>
          </div>
          <div className="property-icon flex-column-center">
            <ul>
              <li className="shadow">
                <a href="#">
                  <img src={bedIcon} alt="icon_bed" />
                  <span className="count">{rentalProperty.bed_count}</span>
                </a>
              </li>
              <li className="shadow">
                <a href="#">
                  <img src={bathIcon} alt="icon_bath" />
                  <span className="count">{rentalProperty.bath_count}</span>
                </a>
              </li>
              <li className="shadow">
                <a href="#">
                  <img src={carIcon} alt="icon_car" />
                  <span className="count">{rentalProperty.car_count}</span>
                </a>
              </li>
            </ul>
          </div>
          <div className="property-title-container flex-column-center">
            <span className="property-title orange large-size medium-font-weight">
              {rentalProperty.title}
            </span>
          </div>
        </div>
        <div className="button-group flex-column-center">
          <div className="text-center">
            <button
              type="button"
              className="btn btn-primary normal-btn min-small-size shadow"
            >
              See all to buy
            </button>
            <button
              type="button"
              className="btn btn-primary normal-btn min-small-size shadow"
            >
              See all to rent
            </button>
          </div>
        </div>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default MobileFeaturedListings;
