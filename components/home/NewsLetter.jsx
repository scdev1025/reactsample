import { Component } from "react";
import styles from "./styles";

const joinClubBg = "../static/images/home/img_joinclub_bg.png";

class NewsLetter extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="intro-joinclub flex-column-center text-center">
        <div
          className="img-background blur-image"
          style={{ backgroundImage: `url(${joinClubBg})` }}
        />
        <div className="orange title">
          <p>BE THE FIRST TO KNOW</p>
        </div>
        <div className="content large-max-size">
          <div className="content-text">Move to the front with real-time</div>
          <div className="">notifications of new properties</div>
        </div>
        <div className="join-form">
          <form className="form-inline">
            <div className="form-group ">
              <input
                type="username"
                className="form-control small-size"
                id="username"
                placeholder="Your name"
              />
            </div>
            <div className="form-group ">
              <input
                type="useremail"
                className="form-control small-size"
                id="useremail"
                placeholder="Your email"
              />
            </div>
            <button type="submit" className="btn btn-primary small-size">
              Join the club
            </button>
          </form>
        </div>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default NewsLetter;
