import { Component } from "react";
import styles from "./styles";

class MobileSearchBar extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="search-bar flex-row-center shadow">
        <div className="input-field">
          <input
            type="postcode"
            className="form-control min-small-size shadow"
            id="postcode"
            placeholder="Suburbs or postcodes"
          />
        </div>
        <div className="buy-select flex-column-center shadow">
          <select className="text-center small-size">
            <option className="black">Buy</option>
            <option className="black">Buy</option>
            <option className="black">Buy</option>
          </select>
          <span className="fas fa-sort-down orange small-size" />
        </div>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default MobileSearchBar;
