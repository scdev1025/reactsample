import { Component } from "react";
import SearchBar from "./SearchBar";
import styles from "./styles";

const propertyBg = "../static/images/home/img_property_bg.png";

class ActionBox extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div
        className="intro flex-column-center img-background"
        style={{ backgroundImage: `url(${propertyBg})` }}
      >
        <div className="sell-property flex-column-center">
          <div className="content text-center medium-size">
            <div className="content-text">Sell your property with</div>
            <div className="">bayside professionals</div>
            <button
              type="submit"
              className="btn btn-primary normal-btn small-size"
            >
              Value my property
            </button>
          </div>
        </div>
        <div className="hello flex-column-center shadow">
          <div className="white medium-large-size">hello</div>
        </div>
        <SearchBar />
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default ActionBox;
