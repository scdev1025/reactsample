import { Component } from "react";
import styles from "./styles";

class SearchBar extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="search-bar flex-column-center">
        <form className="form-inline">
          <div className="buy-select flex-column-center">
            <select className="text-center medium-size">
              <option>Buy</option>
              <option>Buy</option>
              <option>Buy</option>
            </select>
            <span className="fas fa-sort-down orange small-size" />
          </div>
          <div className="form-group">
            <input
              type="postcode"
              className="form-control small-medium-size"
              id="postcode"
              placeholder="Suburbs or postcodes"
            />
          </div>
          <button type="button" className="btn btn-primary small-size">
            <i className="fas fa-search" /> Search
          </button>
        </form>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default SearchBar;
