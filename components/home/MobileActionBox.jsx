import { Component } from "react";
import styles from "./styles";

const propertyBg = "../static/images/home/img_property_bg.png";

class MobileActionBox extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div
        className="intro flex-column-center img-background"
        style={{ backgroundImage: `url(${propertyBg})` }}
      >
        <div className="sell-property-content text-center">
          <div className="white small-size">Sell your property with</div>
          <div className="white small-size">bayside professionals</div>
          <button
            type="submit"
            className="btn btn-primary normal-btn min-small-size shadow"
          >
            Value my property
          </button>
        </div>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default MobileActionBox;
