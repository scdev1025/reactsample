import { Component } from "react";
import styles from "./styles";

class MobileLatestArticles extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { articleList } = this.props;

    return (
      <div className="articles-container flex-column-center">
        <div className="black title medium-size medium-font-weight text-center">
          <p>OUR LATEST ARTICLES</p>
        </div>
        <div className="list">
          <ul>
            {articleList.map((item, index) => (
              <li key={index} className="text-center">
                <a href="#">
                  <div className="orange small-size medium-font-weight">
                    {item.title}
                  </div>
                  <div className="black min-size medium-font-weight font-italic">
                    by {item.author} on {item.date}
                  </div>
                  <div className="bottom-border" />
                </a>
              </li>
            ))}
          </ul>
        </div>
        <div className="article-btn-container">
          <button
            type="button"
            className="btn btn-primary normal-btn min-small-size shadow"
          >
            See all our articles
          </button>
        </div>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default MobileLatestArticles;
