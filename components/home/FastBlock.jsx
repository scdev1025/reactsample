import { Component } from "react";
import styles from "./styles";

class FastBlock extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { fastList } = this.props;

    return (
      <div className="fast-container black-bg">
        <div className="white title text-center">
          <p>FIND IT FAST</p>
        </div>
        <div className="list flex-column-center">
          <ul>
            {fastList.map((item, index) => (
              <li key={index} className="orange-border">
                <a href="#">
                  <div className="content-title orange">{item.title}</div>
                  <div className="white min-small-size text-content">
                    {item.content}
                  </div>
                  <i className="fas fa-arrow-right" />
                </a>
              </li>
            ))}
          </ul>
        </div>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default FastBlock;
