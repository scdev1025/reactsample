import { Component } from "react";
import styles from "./styles";
import {Link} from '../../routes';

class ImageBlock extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      iconImage,
      bgImage,
      title,
      list,
      imagePosition,
      isOffice
    } = this.props;
    return (
      <div className="choose container-fluid">
        {imagePosition == "right" ? (
          <div className="list flex-column-center">
            <ul>
              {list.map((item, index) => (
                <li key={index}>
                  {isOffice ? (
                    <a href="#">
                      <div className="content-title orange">{item.office}</div>
                      <div className="black min-small-size">{item.address}</div>
                      <i className="fas fa-arrow-right" />
                    </a>
                  ) : (
                    <a href="#">
                      <div className="content-title orange">{item}</div>
                      <i className="fas fa-arrow-right" />
                    </a>
                  )}
                </li>
              ))}
            </ul>
          </div>
        ) : (
          <div
            className="image img-background"
            style={{ backgroundImage: `url(${bgImage})` }}
          >
            <Link href={this.props.link}>
              <div className="content flex-column-center">
                <img src={iconImage} alt="icon_people" />
                <div className="white title">{title}</div>
              </div>
            </Link>
          </div>
        )}
        {imagePosition == "right" ? (
          <div
            className="image img-background"
            style={{ backgroundImage: `url(${bgImage})` }}
          >
            <Link href={this.props.link}>
              <div className="content flex-column-center">
                <img src={iconImage} alt="icon_house" />
                <div className="white title">{title}</div>
              </div>
            </Link>
          </div>
        ) : (
          <div className="list flex-column-center">
            <ul>
              {list.map((item, index) => (
                <li key={index}>
                  {isOffice ? (
                    <a href="#">
                      <div className="content-title orange">{item.office}</div>
                      <div className="black min-small-size">{item.address}</div>
                      <i className="fas fa-arrow-right" />
                    </a>
                  ) : (
                    <a href="#">
                      <div className="content-title orange">{item}</div>
                      <i className="fas fa-arrow-right" />
                    </a>
                  )}
                </li>
              ))}
            </ul>
          </div>
        )}
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default ImageBlock;
