import css from "styled-jsx/css";

export default css`
  .intro {
    height: 95vh;
  }
  .intro .sell-property {
    padding-top: 100px;
    flex: 5;
    width: 100%;
  }
  .intro .sell-property .content {
    background: rgba(9, 0, 0, 0.4);
    padding: 30px 0px;
    width: 480px;
    height: 200px;
    margin-top: 10vh;
    font-weight: 500;
    color: white;
  }
  .intro .sell-property .content .content-text {
    margin-bottom: -10px;
  }
  .intro .sell-property .content .btn {
    margin-top: 20px;
  }
  .intro .hello {
    position: absolute;
    background: #ff8124;
    transform: translate(-260px, -130px);
    width: 130px;
    height: 130px;
    border-radius: 100%;
    text-align: center;
    font-weight: 500;
  }
  .intro .search-bar {
    flex: 2;
    width: 100%;    
  }
  .intro .search-bar .form-inline {
    width: 100%;
    justify-content: center;
  }
  .intro .search-bar .form-group {
    width: 40%;
  }
  .intro .search-bar .form-control {
    border: white 1px solid;
    border-radius: 0;
    padding: 32px 10px !important;
    background-color: white;
    margin: 0px;
    color: black;
    width: 100%;
  }
  .buy-select {
    background: white;
    width: 150px;
    padding: 9px 0px 9px 20px;
  }
  .buy-select select {
    color: #ff8124;
    background: transparent;
    border: none;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    width: 100%;
  }
  .buy-select select:focus {
    outline: none;
  }
  .buy-select span {
    position: absolute;
    margin-left: 20px;
    transform: translateX(20px);
  }

  .intro-property {
    height: 500px;
    width: 100%;
    display: flex;
  }
  .intro-property .col-md-4 {
    padding-right: 0;
    padding-left: 0;
  }
  .best-property {
    background-color: #f3f3f3;
  }
  .best-property .title {
    margin-top: 50px;
    display: flex;
    justify-content: flex-end;
    align-items: flex-end;
    font-size: 2.25rem;
  }
  .best-property .content {
    flex: 1;
    margin: 10px;
  }
  .best-property .button-group {
    margin-bottom: 50px;
  }
  .property-button {
    margin: 10px;    
  }
  .property-img {
    height: 70%;
  }
  .property-desc {
    padding: 15px;
    background: rgb(255, 129, 36, 0.7);
    width: 100%;
  }
  .property-title-container {
    height: 30%;
    background: #fff;
  }
  .property-title {
    margin-top: 30px;
    font-size: 2.25rem;
    font-weight: 500;
  }

  .intro-joinclub {
    height: 400px;
  }
  .intro-joinclub .title{
    font-size: 2.25rem;
  }
  .intro-joinclub .content {
    color: #fff;
    font-weight: 500;
    margin-bottom: 30px;
  }
  .intro-joinclub .content .content-text {
    margin-bottom: -20px;
  }
  .join-form .form-control {
    border: #fff 1px solid;
    border-radius: 0;
    // font-size: 2rem;
    padding: 32px 10px !important;
    background-color: transparent;
    margin: 0px 10px 0px 0px;
    color: #fff;
  }
  .black-bg {
    background-color: #221e1f;
    width: 35%;
  }
  .articles-container {
    flex: 1;
    background-color: #fff;
  }
  .articles-container .list {
    flex: 1;
  }

  .choose {
    display: flex;
    height: 400px;
    margin-bottom: 50px;
  }
  .choose .list {
    flex: 3;
  }
  .choose .image {
    flex: 5;
  }
  .choose .image .content {
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.3);
  }
  .choose .image .content:hover {
    background-color: rgba(0, 0, 0, 0.7);
    cursor: pointer;
  }
  .choose .image .content .title {
    margin-top: 5px;
  }
  .choose .title {
    margin: 30px 0px 0px 0px;
  }
  .choose .black-text {
    margin: 20px 0px 20px 0px;
  }
  .choose .list ul {
    width: 70%;
  }

  .fast-container .title,
  .articles-container .title {
    font-size: 2rem;
    margin: 30px 0px 20px 0px;
  }
  .fast-container .list ul {
    width: 85%;
  }
  .list ul li.orange-border {
    border-left: 4px solid #ff8124;
    padding: 10px 0 5px 15px;
  }
  .list ul li.gray-border {
    border-left: 5px solid #f3f3f3;
    padding: 10px 0 5px 15px;
  }
  .article-btn-container {
    margin: 50px 10px;
  }
  .article-content {
    padding: 20px 0px;
  }

  .btn {
    border: none;
    border-radius: 0;
    background: #ff8124;
    color: #fff;
    padding: 15px 20px;
  }
  .normal-btn {
    padding: 10px 20px;
  }
  .list li {
    margin-bottom: 20px;
  }

  @media screen and (max-width: 767px) {
    .intro {
      padding-top: 0px;
      height: 300px;
    }
    .intro .sell-property-content {
      background: rgba(9, 0, 0, 0.4);
      padding: 30px 30px;
      margin: 0px 20px;
    }
    .intro .sell-property-content .btn {
      margin-top: 10px;
    }

    .search-bar {
      background: #f3f3f3;
      padding: 50px 10px 20px 10px;
    }
    .search-bar .input-field {
      flex: 1;
      height: 100%;
    }
    .search-bar .form-control {
      border: white 1px solid;
      border-radius: 0;
      padding: 25px 10px !important;
      background-color: white;
      color: black;
      width: 100%;
    }
    .buy-select {
      background: #ff8124;
      width: 100px;
      padding: 8px 0px 8px 10px;
    }
    .buy-select select {
      color: white;
      background: transparent;
      border: none;
      -webkit-appearance: none;
      -moz-appearance: none;
      appearance: none;
      width: 100%;
    }
    .buy-select select:focus {
      outline: none;
    }
    .buy-select span {
      position: absolute;
      color: white;
      margin-left: 30px;
      transform: translateY(-3px);
    }

    .intro-property {
      height: 1000px;
      display: flex;
      background: #f3f3f3;
      flex-direction: column;
      justify-content: center;
      // align-items: center;
    }
    .intro-property .property {
      height: 40%;
    }
    .property-img {
      height: 75%;
    }
    .property-title-container {
      height: 25%;
      background: #fff;
    }
    .property-desc {
      padding: 5px;
      background: rgb(255, 129, 36, 0.7);
    }
    .intro-property .title-container {
      height: 10%;
    }
    .intro-property .title-container .title {
      font-size: 1.8rem;
      font-weight: 500;
    }
    .intro-property .button-group {
      height: 10%;
    }
    .intro-property .button-group .btn {
      margin: 10px 5px;
    }
    .intro-joinclub {
      height: 450px;
    }
    .intro-joinclub .title {
      margin-top: 20px;
    }
    .intro-joinclub .content,
    .join-form {
      padding-left: 20px;
      padding-right: 20px;
      width: 80%;
    }
    .join-form .form-control {
      border: #fff 1px solid;
      border-radius: 0;
      padding: 20px 10px !important;
      background-color: transparent;
      color: #fff;
      width: 100%;
    }
    .join-form .btn {
      width: 100%;
    }
    .fast-container .title,
    .articles-container .title {
      margin: 20px 0px;
      font-size: 1.75rem;
    }
    .fast-container .list ul {
      width: 90%;
      margin-bottom: 10px;
    }
    .fast-container .list ul li {
      margin-bottom: 15px;
    }
    .fast-container {
      width: 100%;
      height: 45%;
    }
    .articles-container {
      width: 100%;
      height: 55%;
    }
    .articles-container .list ul li {
      padding: 0px 15px 0px 15px;
    }
    .list ul li .bottom-border {
      height: 5px;
      background: #f3f3f3;
      margin: 15px 30px;
    }
    .articles-container .list {
      flex: none;
    }
    .article-btn-container {
      margin: 20px;
    }
    .choose-content {
      margin: 10px 0px;
    }
    .choose-content img {
      margin-bottom: 10px;
    }
  }
`;
