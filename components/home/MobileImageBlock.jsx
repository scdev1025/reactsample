import { Component } from "react";
import styles from "./styles";
import {Link} from '../../routes';

class MobileImageBlock extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { iconImage, title } = this.props;
    return (
      <Link href={this.props.link}>
        <div className="choose-content flex-column-center">
          <img src={iconImage} alt="icon_marker" />
          <div className="black small-size medium-font-weight">{title}</div>
          <style jsx>{styles}</style>
        </div>
      </Link>
    );
  }
}

export default MobileImageBlock;
