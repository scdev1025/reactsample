import { Component } from "react";
import styles from "./styles";

class MobileFastBlock extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { fastList } = this.props;

    return (
      <div className="fast-container black-bg">
        <div className="white title medium-size medium-font-weight text-center">
          <p>FIND IT FAST</p>
        </div>
        <div className="list flex-column-center text-center">
          <ul>
            {fastList.map((item, index) => (
              <li key={index}>
                <a href="#">
                  <div className="orange small-size">{item.title}</div>
                </a>
              </li>
            ))}
          </ul>
        </div>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default MobileFastBlock;
