import { Component } from "react";
import styles from "./styles";

const joinClubBg = "../static/images/home/img_joinclub_bg.png";

class MobileNewsLetter extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="intro-joinclub flex-column-center text-center">
        <div
          className="img-background blur-image"
          style={{ backgroundImage: `url(${joinClubBg})` }}
        />
        <div className="orange small-medium-size medium-font-weight text-center">
          <p>BE THE FIRST TO KNOW</p>
        </div>
        <div className="content white small-size text-center">
          <div>
            Move to the front with real-time notifications of new properties
          </div>
        </div>
        <div className="join-form">
          <div className="form-group">
            <input
              type="username"
              className="form-control min-small-size text-center"
              id="username"
              placeholder="Your name"
            />
          </div>
          <div className="form-group ">
            <input
              type="useremail"
              className="form-control min-small-size text-center"
              id="useremail"
              placeholder="Your email"
            />
          </div>
          <button
            type="submit"
            className="btn btn-primary normal-btn min-small-size shadow"
          >
            Join the club
          </button>
        </div>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default MobileNewsLetter;
