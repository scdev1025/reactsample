import css from "styled-jsx/css";

export default css`
  .office {
    margin-bottom: 50px;
    height: 500px;
    max-width: 1000px;
    width: 80%;
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
  }
  .office-info {
    height: 70px;
    margin-bottom: 20px;
  }
  .office-title {
    height: 100%;
    background: #ff8124;
    padding: 0px 20px;
  }
  .office-address-container {
    background: white;
    height: 100%;
    align-items: flex-start;
    padding: 0px 20px;
  }

  @media screen and (max-width: 767px) {
    .office {
      border-radius: 5px;
      margin-bottom: 50px;
      height: auto;
      width: 100%;
      display: flex;
      flex-direction: column;
      justify-content: flex-start;
      background: white;
    }
    .image {
      height: 300px;
      width: 100%;
    }
    .office-content {
      padding: 20px;
    }
    .normal-btn {
      border: none;
      border-radius: 0;
      background: #ff8124;
      color: #fff;
      margin-top: 10px;
      padding: 10px 15px;
    }
  }
`;
