import { Component } from "react";
import styles from "./styles";

class MobileOfficeCard extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { office } = this.props;

    return (
      <div className="office shadow">
        <div
          className="image img-background"
          style={{
            backgroundImage: `url(${office.image})`
          }}
        />
        <div className="office-content">
          <div className="orange small-medium-size medium-font-weight">
            {office.title}
          </div>
          <div className="black min-small-size">
            {office.address}
          </div>
          <div className="orange min-small-size">
            T <span className="black font-italic">{office.phone}</span>
          </div>
          <button
            type="button"
            className="btn btn-primary normal-btn min-small-size shadow"
          >
            View Office
          </button>
        </div>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default MobileOfficeCard;
