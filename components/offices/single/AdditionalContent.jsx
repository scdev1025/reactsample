import { Component } from "react";
import styles from "./styles";
import { arrowDownIcon } from "../../shared/variables";

const imgMapView = "../../../static/images/properties/img_map_view.png";

class AdditionalContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mapViewIndex: 0
    };
  }

  onPressMapView(viewIndex) {
    this.setState({
      mapViewIndex: viewIndex
    });
  }

  render() {
    const { office } = this.props;
    const { mapViewIndex } = this.state;

    return (
      <div className="additional-content">
        <div className="opening-hours">
          <div className="heading">OPENING HOURS</div>
          <div className="hour-list">
            {office.openingHours.map((item, index) => (
              <div key={index} className="opening-item">
                <div className="weekday"> {item.weekday} </div>
                <div className="hours"> {item.hours} </div>
              </div>
            ))}
          </div>
        </div>
        <div className="view-section">
          <div
            className="img-background"
            style={{ backgroundImage: `url(${imgMapView})` }}
          >
            <button
              type="button"
              className="btn btn-primary btn-direction shadow"
            >
              Get directions
            </button>
            <div className="btn-group" role="group">
              <button
                type="button"
                className={mapViewIndex == 0 ? "btn selected" : "btn"}
                onClick={() => this.onPressMapView(0)}
              >
                Map view
              </button>
              <img
                src={arrowDownIcon}
                className={mapViewIndex == 0 ? "img-selected" : ""}
              />
              <button
                type="button"
                className={mapViewIndex == 1 ? "btn selected" : "btn"}
                onClick={() => this.onPressMapView(1)}
              >
                Street View
              </button>
              <img
                src={arrowDownIcon}
                className={mapViewIndex == 1 ? "img-selected" : ""}
              />
            </div>
          </div>
        </div>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default AdditionalContent;
