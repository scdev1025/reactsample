import { Component } from "react";
import styles from "./styles";
import ReadMoreButton from "../../shared/ReadMoreButton";
import { playRoundIcon } from "../../shared/variables";

class MainContent extends Component {
  render() {
    const {office} = this.props;

    return (
      <div className="main-content">
        <div className="heading">
          <div className="address-info">
            <div className="address">{office.address}</div>
            <div className="email">
              <span className="orange">E</span> {office.email}
            </div>
          </div>
          <div className="contact-info">
            <div className="phone">
              <span className="orange">T</span> {office.phone}
            </div>
            <div className="fax">
              <span className="orange">F</span> {office.fax}
            </div>
          </div>
        </div>
        <div className="content">
          <p>
            Porta ullamacorper fringilla quis scelerisque natoque habitant erat
            tincidunt dolor ullamocrper consectetur feugiant parturient labortis
            semper vestibulum vitae senectus inceptos proin class. Scelerisque a
            malesuada luctus est elementum ante ultrices quam habitant dui sed
            non erat ridiculus justo ac suscipit consequat a justo scelerisque.
            Tempus rhoncus vestibulum a felis vivamus accumsan in suscipit felis
            in dictumst a vulputate dis dodio orci adipiscing aptent condimentum
            sodales eget arcu. Velit fringilla nunc mi tristique pharetra nec
            est inceptos cubilia nascetur ac cum consectetur dui per.
          </p>
          <div className="readmore">
            <ReadMoreButton />
          </div>
          <p className="more-content">
            Porta ullamacorper fringilla quis scelerisque natoque habitant erat
            tincidunt dolor ullamocrper consectetur feugiant parturient labortis
            semper vestibulum vitae senectus inceptos proin class. Scelerisque a
            malesuada luctus est elementum ante ultrices quam habitant dui sed
            non erat ridiculus justo ac suscipit consequat a justo scelerisque.
            Tempus rhoncus vestibulum a felis vivamus accumsan in suscipit felis
            in dictumst a vulputate dis dodio orci adipiscing aptent condimentum
            sodales eget arcu. Velit fringilla nunc mi tristique pharetra nec
            est inceptos cubilia nascetur ac cum consectetur dui per.
          </p>
        </div>
        <div className="video-view">
          <div
            className="img-background"
            style={{
              backgroundImage: `url(${"../../static/images/offices/img_office_elwood.png"})`
            }}
          >
            <div className="content flex-row-center">
              <img src={playRoundIcon} />
            </div>
          </div>
        </div>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default MainContent;
