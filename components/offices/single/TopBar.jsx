import { Component } from "react";
import styles from "./styles";
import {
  facebookIcon,
  twitterIcon,
  instagramIcon
} from "../../shared/variables";

class TopBar extends Component {
  render() {
    const { office, contactAgentAction, isContactAgent } = this.props;

    return (
      <div className="top-bar">
        <div className="center-title">{office.title}</div>
        <div className="left-content social-icon">
          <ul>
            <li>
              <a href="#">
                <img src={facebookIcon} alt="icon_facebook" />
              </a>
            </li>
            <li>
              <a href="#">
                <img src={twitterIcon} alt="icon_twitter" />
              </a>
            </li>
            <li>
              <a href="#">
                <img src={instagramIcon} alt="icon_instagram" />
              </a>
            </li>
          </ul>
        </div>
        <div className="right-content">
          <a className="btn btn-default" onClick={contactAgentAction}>
            {isContactAgent ? "Cancel" : "Contact the team"}
          </a>
        </div>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default TopBar;
