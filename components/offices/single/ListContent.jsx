import { Component } from "react";
import styles from "./styles";
import PeopleItem from "../../shared/PeopleItem";
import PropertyList from "./PropertyList";
import Testimonial from "../../shared/Testimonial";

class ListContent extends Component {
  render() {
    return (
      <div className="list-content">
        <div className="agent-list">
          <div className="heading">
            OUR AGENTS
            <div className="see-all">
              <a className="btn btn-default" title="See them all">
                See them all
              </a>
            </div>
          </div>
          <div className="list row">
            <div className="col-md-6">
              <PeopleItem />
              <PeopleItem />
              <PeopleItem />
            </div>
            <div className="col-md-6">
              <PeopleItem />
              <PeopleItem />
              <PeopleItem />
            </div>
          </div>
        </div>
        <div className="property-list">
          <div className="sale-list">
            <PropertyList title="OUR PROPERTIES FOR SALE" />
          </div>
          <div className="rent-list">
            <PropertyList title="OUR PROPERTIES FOR RENT" />
          </div>
        </div>
        <div className="testimonial">
          <Testimonial />
        </div>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default ListContent;
