import css from "styled-jsx/css";

export default css`
  .top-bar {
    padding-top: 100px;
    background: #ff8124;
    -webkit-box-shadow: 0px 9px 9px -5px rgba(0, 0, 0, 0.31);
    -moz-box-shadow: 0px 9px 9px -5px rgba(0, 0, 0, 0.31);
    box-shadow: 5px 9px 9px -5px rgba(0, 0, 0, 0.31);
    position: relative;
    z-index: 1;    
    padding-right: 20px;
    padding-left: 20px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 170px;
  }
  .top-bar .center-title {
    position: absolute;
    left: 0;
    color: white;
    font-size: 2rem;
    font-weight: 500;
    text-align: center;
    width: 100%;
  }
  .top-bar .right-content {
    z-index: 1;
  }
  .top-bar .right-content .btn {
    -webkit-box-shadow: 2px 2px 5px 2px rgba(0, 0, 0, 0.21);
    -moz-box-shadow: 2px 2px 5px 2px rgba(0, 0, 0, 0.21);
    box-shadow: 2px 2px 5px 2px rgba(0, 0, 0, 0.21);
    padding: 10px 27px;
    background: #fff;
    color: black;
    font-size: 1.25rem;
    border-radius: 0;
    margin-bottom: 0px;
  }
  .top-bar .social-icon {
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
  }
  .top-bar .social-icon ul li {
    display: inline-block;
    margin-left: 10px;
  }
  .top-bar .social-icon ul li a {
    display: block;
    max-width: 56px;
    max-height: 52px;
  }

  .main-content {
    margin-left: -15px;
    margin-right: -15px;
    margin-top: 20px;
  }
  .main-content .heading {
    padding: 20px;
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
  .main-content .heading .address {
    font-size: 1.75rem;
    font-weight: 500;
  }
  .main-content .heading .email {
    font-size: 1.5rem;
    font-weight: 300;
  }
  .main-content .heading .contact-info {
    display: flex;
    flex-direction: column;
    align-items: flex-end;
  }
  .main-content .heading .phone {
    font-size: 1.75rem;
    font-weight: 500;
  }
  .main-content .heading .fax {
    font-size: 1.5rem;
    font-weight: 300;
  }

  .main-content .content {
    font-size: 1.35rem;
    background: white;
    padding: 20px;
  }
  .main-content .content .readmore {
    display: none;
  }

  .video-view {
    height: 600px;
  }
  .video-view .img-background {
    width: 100%;
    height: 100%;
  }
  .video-view .img-background .content {
    width: 100%;
    height: 100%;
    background: transparent;
  }
  .video-view .img-background .content:hover {
    background-color: rgba(0, 0, 0, 0.7);
    cursor: pointer;
  }

  .additional-content {
    margin-top: 20px;
  }
  .additional-content .opening-hours {
    padding: 20px;
    margin-bottom: 20px;
  }
  .additional-content .opening-hours .heading {
    color: #ff8214;
    font-size: 1.75rem;
    font-weight: 500;
    margin-bottom: 20px;
  }
  .additional-content .opening-hours .opening-item {
    font-size: 1.5rem;
    display: flex;
  }
  .additional-content .opening-hours .opening-item .weekday {
    color: #ff8214;
    font-weight: 500;
    width: 150px;
    margin-right: 10px;
  }
  .additional-content .view-section {
    margin-left: -15px;
    margin-right: -15px;
  }
  .additional-content .view-section .img-background {
    height: 500px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
  }
  .additional-content .view-section .btn-direction {
    background: #ff8124;
    border: none;
    border-radius: 0;
    font-size: 1.25rem;
    padding: 5px 20px;
    margin-left: 20px;
    margin-top: 20px;
    width: 200px;
  }
  .additional-content .view-section .btn-group {
    background: white;
    height: 40px;
    width: 100%;
  }
  .additional-content .view-section .btn-group .btn {
    background: white;
    border-radius: 0px;
    height: 100%;
    font-size: 1.25rem;
    color: #ff8214;
    width: 50%;
  }
  .additional-content .view-section .btn-group .selected {
    color: white;
    background: #ff8214;
  }
  .additional-content .view-section .btn-group img {
    position: absolute;
    transform: translate(-550%, -90%) rotate(180deg);
    display: none;
    width: 5%;
  }
  .additional-content .view-section .btn-group img.img-selected {
    display: inline;
  }

  .list-content {
    margin-left: -15px;
    margin-right: -15px;
  }
  .agent-list .heading {
    background: #ff8214;
    color: white;
    font-size: 2rem;
    margin: 0px;
    padding: 0.625rem 1.75rem;
    margin-bottom: 20px;
  }
  .agent-list .heading .see-all {
    display: none;
  }
  .agent-list .list {
    padding: 0px 20px;
  }

  .property-list {
    padding-top: 20px;
  }

  .property-list .see-all .btn {
    -webkit-box-shadow: 2px 2px 5px 2px rgba(0, 0, 0, 0.21);
    -moz-box-shadow: 2px 2px 5px 2px rgba(0, 0, 0, 0.21);
    float: right;
    box-shadow: 2px 2px 5px 2px rgba(0, 0, 0, 0.21);
    background: #fff;
    color: #221e1f !important;
    font-size: 1.5rem;
    max-width: 177px !important;
    min-width: 177px !important;
    border-radius: 0;
    margin-right: 10px;
  }

  .property-list .block-heading {
    background: #ff8124;
    margin-left: -15px;
    margin-right: -15px;
    padding: 0.625rem 1.75rem;
    margin-bottom: 30px;
    display: flex;
    flex-direction: row;
  }

  .property-list .block-heading .heading-title {
    margin-top: 10px;
    color: #fff;
    font-size: 1.75rem;
    width: 50%;
  }
  .property-list .block-heading .heading-button {
    width: 50%;
  }

  .property-list .listing-items {
    padding-left: 10px;
    padding-right: 20px;
  }

  .list-content .testimonial {
    background: white;
    padding: 30px 0px;
  }

  @media screen and (max-width: 767px) {
    .top-bar {
      padding: 10px 10px;
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
    }
    .top-bar .center-title {
      position: relative;
      font-size: 1.5rem;
    }
    .top-bar .left-content {
      height: 70px;
      display: flex;
      flex-direction: row;
      justify-content: center;
      align-items: center;
    }
    .top-bar .left-content .property-icon {
      display: flex;
      flex-direction: row;
      justify-content: center;
      align-items: center;
      transform: translateY(0%);
    }
    .top-bar .right-content {
      padding: 10px;
    }
    .top-bar .right-content .btn {
      width: 250px;
      font-size: 1.25rem;
      margin-bottom: 0px;
    }

    .main-content {
      margin-left: 0px;
      margin-right: 0px;
      margin-top: 0px;
    }
    .main-content .heading {
      flex-direction: column;
      align-items: flex-start;
    }
    .main-content .heading .email {
      font-size: 1.25rem;
    }
    .main-content .heading .contact-info {
      align-items: flex-start;
    }
    .main-content .heading .phone {
      font-size: 1.5rem;
    }
    .main-content .heading .fax {
      font-size: 1.25rem;
    }
    .main-content .heading .orange {
      font-size: 1.75rem;
      font-weight: 500;
    }

    .main-content .content {
      font-size: 1.25rem;
    }
    .main-content .content .readmore {
      display: inherit;
    }
    .more-content {
      display: none;
    }
    .video-view {
      height: 300px;
    }

    .additional-content {
      margin-bottom: 20px;
    }
    .additional-content .opening-hours .heading {
      font-size: 1.5rem;
    }
    .additional-content .opening-hours .opening-item {
      font-size: 1.25rem;
    }
    .additional-content .opening-hours .opening-item .weekday {
      width: 120px;
    }

    .list-content {
      margin-left: 0px;
      margin-right: 0px;
    }
    .agent-list .list {
      padding: 0px;
    }
    .agent-list .heading {
      font-size: 1.75rem;
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
    }
    .agent-list .heading .see-all {
      display: inherit;
    }

    .property-list .block-heading {
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
    }
    .property-list .block-heading .heading-title {
      width: 100%;
      font-size: 1.75rem;
      text-align: center;
    }
    .property-list .block-heading .heading-button {
      width: auto;
      text-align: center;
    }
    .property-list .see-all .btn,
    .agent-list .heading .see-all .btn {
      -webkit-box-shadow: 2px 2px 5px 2px rgba(0, 0, 0, 0.21);
      -moz-box-shadow: 2px 2px 5px 2px rgba(0, 0, 0, 0.21);
      box-shadow: 2px 2px 5px 2px rgba(0, 0, 0, 0.21);
      background: #fff;
      color: #221e1f !important;
      font-size: 1.25rem;
      max-width: 177px !important;
      min-width: 177px !important;
      border-radius: 0;
      margin: 20px 10px;
    }
    .list-content .testimonial {
      background: #efefef;
      padding: 30px 0px;
    }
    .property-list .listing-items {
      padding-left: 0px;
      padding-right: 0px;
    }
  }
`;
