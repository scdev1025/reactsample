import { Component } from "react";
import styles from "./styles";

class OfficeCard extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { office } = this.props;

    return (
      <div
        className="office img-background"
        style={{
          backgroundImage: `url(${office.image})`
        }}
      >
        <div className="office-info flex-row-center">
          <div className="office-title white small-medium-size medium-font-weight flex-column-center">
            {office.title}
          </div>
          <div className="office-address-container flex-column-center">
            <div className="office-address black min-small-size medium-font-weight">
              {office.address}
            </div>
            <div className="office-phone orange min-small-size medium-font-weight">
              T <span className="black font-italic">{office.phone}</span>
            </div>
          </div>
        </div>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default OfficeCard;
