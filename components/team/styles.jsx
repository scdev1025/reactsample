import css from "styled-jsx/css";

export default css`
  .search-container .buttons {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
  }
  .search-container .buttons .search-options {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
  }
  .btn-search {
    border: none;
    border-radius: 0;
    background: #ff8124;
    color: #fff;
    width: 90%;
    padding: 1.2rem;
    font-size: 1.5rem;
  }
  .sortby-container {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
  .sortby-container .heading {
    font-size: 2rem;
    font-weight: 500;
  }

  .check-office {
    height: 400px;
  }
  .check-office .transparent-bg {
    width: 100%;
    height: 100%;
    background: rgba(0, 0, 0, 0.5);
  }
  .check-office .title {
    font-size: 2rem;
  }
  .check-office .content-text {
    color: #fff;
    font-weight: 500;
    font-size: 2.75rem;
  }
  .check-office a {
    display: flex;
    align-items: center;
    margin-top: 50px;
  }
  .check-office a .maker-icon {
    width: 30px;
  }
  .check-office a .content {
    font-size: 1.5rem;
    margin: 0px 20px 0px 10px;
  }

  @media screen and (max-width: 767px) {
    .search-container .buttons {
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
    }
    .search-container .buttons .search-options {
      flex-direction: column;
    }
    .search-container .btn-search {
      padding: 0.75rem;
    }

    .check-office .title {
      font-size: 2.25rem;
    }
    .check-office .content-text {
      color: #fff;
      font-weight: 500;
      font-size: 1.75rem;
      padding: 0px 10px;
    }

    .sortby-container {
      flex-direction: column;
      align-items: center;
      margin-top: 20px;
    }
    .sortby-container .heading {
      font-size: 1.25rem;
      font-weight: 500;
    }
    .sortby-container .heading .agent-count {
      font-size: 2rem;
    }
  }
`;
