import { Component } from "react";
import TitleSelect from "../shared/TitleSelect";
import SortBySelect from "../shared/SortBySelect";
import styles from "./styles";
import StyledInput from "../shared/StyledInput";

class SearchContainer extends Component {
  render() {
    const { agentCount } = this.props;

    return (
      <div className="search-container">
        <div className="buttons flex-row-between">
          <div className="search-options col-md-10">
            <div className="col-md-6 p-1 m-0">
              <StyledInput title="Agent Name" text="Sam Gamon" />
            </div>
            <div className="col-md-6 p-0 m-0 flex-row-between">
              <div className="col-md-6 p-1 m-0">
                <TitleSelect title="Office" list="Bayside" />
              </div>
              <div className="col-md-6 p-1 m-0">
                <TitleSelect title="Position" list="Director" />
              </div>
            </div>
          </div>
          <div className="col-md-2 p-0 m-0 flex-row-center">
            <button type="button" className="btn btn-primary btn-search shadow">
              Search
            </button>
          </div>
        </div>
        <div className="sortby-container col-md-12">
          <div className="heading orange">
            <span className="agent-count black">{agentCount}</span> Bayside Real
            Estate Agents
          </div>
          <SortBySelect />
        </div>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default SearchContainer;
