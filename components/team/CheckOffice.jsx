import { Component } from "react";
import styles from "./styles";
import { markerIcon } from "../shared/variables";

const officeBG = "../static/images/offices/img_office_elwood.png";

class CheckOffice extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="check-office flex-column-center text-center">
        <div
          className="img-background blur-image"
          style={{ backgroundImage: `url(${officeBG})` }}
        >
          <div className="transparent-bg" />
        </div>
        <div className="orange title">CAN'T FIND WHO YOU'RE LOOKING FOR?</div>
        <div className="content-text">Get in touch with one of our offices</div>
        <a>
          <img className="maker-icon" src={markerIcon} alt="makerIcon" />
          <span className="content white">{"Find an office near you"}</span>
          <i className="fas fa-arrow-right white" />
        </a>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default CheckOffice;
