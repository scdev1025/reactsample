import styles from "./styles";
import ListingItem from "../shared/ListingItem";

const LatestListing = props => {
  const { title } = props;
  return (
    <section className="latest-listing">
      <div className="block-heading">
        <div className="heading-title medium-font-weight">{title}</div>
        <div className="heading-button">
          <div className="see-all">
            <a className="btn btn-default" title="See them all">
              See them all
            </a>
          </div>
        </div>
      </div>
      <div className="listing-items">
        <ListingItem />
        <ListingItem />
        <ListingItem />
      </div>
      <style jsx>{styles}</style>
    </section>
  );
};

export default LatestListing;
