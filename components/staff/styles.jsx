import css from "styled-jsx/css";

export default css`
  .top-profile-detail-bar {
    padding-top: 100px;
    background: #ff8124;
    -webkit-box-shadow: 0px 9px 9px -5px rgba(0, 0, 0, 0.31);
    -moz-box-shadow: 0px 9px 9px -5px rgba(0, 0, 0, 0.31);
    box-shadow: 5px 9px 9px -5px rgba(0, 0, 0, 0.31);
    position: relative;
    z-index: 1;
  }
  .top-profile-detail-bar h1 {
    line-height: 36px;
  }
  .top-profile-detail-bar h1,
  .top-profile-detail-bar .designation {
    margin: 0;
    padding: 0;
    font-size: 1.75rem;
    font-family: "museo", serif;    
    color: #fff;
  }  
  .top-profile-right-detail {
    line-height: 38px;
  }
  .top-profile-detail-bar .designation {
    font-size: 1.25rem;    
    margin-left: 10px;
  }
  .top-profile-right-detail .contact {
    margin-right: 37px;
  }
  .top-profile-right-detail .contact,
  .top-profile-right-detail .contact-agent {
    display: inline-block;
    vertical-align: middle;
  }
  .top-profile-right-detail .contact-agent .btn {
    -webkit-box-shadow: 2px 2px 5px 2px rgba(0, 0, 0, 0.21);
    -moz-box-shadow: 2px 2px 5px 2px rgba(0, 0, 0, 0.21);
    box-shadow: 2px 2px 5px 2px rgba(0, 0, 0, 0.21);
    padding: 10px 27px;
    background: #fff;
    color: black;
    font-size: 1.25rem;
    border-radius: 0;
  }
  .top-profile-right-detail .contact .phone {
    margin-top: 3px;
  }
  .top-profile-right-detail .contact .phone,
  .top-profile-right-detail .contact .fax {
    font-size: 1.75rem;
    font-weight: 500;
    color: #fff;
    font-family: "museo", serif;
  }
  .top-profile-right-detail .contact .fax {
    margin-top: -4px;
  }
  .top-profile-right-detail .contact .fax,
  .top-profile-right-detail .contact .fax span {
    font-size: 1.5rem;
    font-family: "museo-sans", sans-serif !important;
    font-weight: 300 !important;
  }
  .top-profile-right-detail .contact .phone span,
  .top-profile-right-detail .contact .fax span {
    color: #221e1f;
    margin-right: 5px;
  }

  .form-control {
    padding: 0.375rem 1.3125rem;
  }

  select::-ms-expand {
    display: none;
  }

  .latest-listing-sold {
    margin-top: 3px;
    padding-top: 0;
  }

  .see-all .btn {
    -webkit-box-shadow: 2px 2px 5px 2px rgba(0, 0, 0, 0.21);
    -moz-box-shadow: 2px 2px 5px 2px rgba(0, 0, 0, 0.21);
    float: right;
    box-shadow: 2px 2px 5px 2px rgba(0, 0, 0, 0.21);
    background: #fff;
    color: #221e1f !important;
    font-size: 1.5rem;
    max-width: 177px !important;
    min-width: 177px !important;
    border-radius: 0;
    margin-right: 10px;
  }

  .latest-listing {
    padding-top: 30px;
  }

  .block-heading {
    background: #ff8124;
    margin-left: -15px;
    margin-right: -15px;
    padding: 0.625rem 1.5rem;
    margin-bottom: 30px;
    display:flex;
    flex-direction: row;
  }

  .block-heading .heading-title {
    margin-top: 10px;
    color: #fff;
    font-size: 1.5rem;
    width: 50%;
  }
  .block-heading .heading-button {
    width: 50%;
  }

  .listing-items {
    margin-left: 10px;
  }

  @media screen and (max-width: 767px) {
    .top-profile-detail-bar h1 {
      font-size: 1.5rem;
    }
    .top-profile-detail-bar .designation {
      font-size: 1.25rem;
      font-weight: 300;
    }
    .top-profile-detail-bar {
      padding: 1.375rem 0 1.375rem 0;
      text-align: center;
    }
    .top-profile-right-detail .contact .phone,
    .top-profile-right-detail .contact .fax,
    .top-profile-right-detail .contact .fax span {
      font-size: 1.25rem;
      font-weight: 400 !important;
      width: 100% !important;
      text-align: center !important;
    }
    .top-profile-right-detail {
      width: 100% !important;
      text-align: center;
    }
    .top-profile-detail-bar h1 {
      line-height: 17px;
    }
    .top-profile-right-detail {
      line-height: 29px;
    }
    .top-profile-right-detail .contact {
      margin-right: 0px;
    }
    .top-profile-right-detail .contact .fax {
      float: none !important;
    }
    .top-profile-right-detail .contact,
    .top-profile-right-detail .contact-agent {
      display: block;
      vertical-align: middle;
    }
    .top-profile-right-detail .contact-agent .btn {
      width: 90%;
      margin-top: 10px;
      font-size: 1.25rem;
    }
    .top-profile-detail-bar {
      box-shadow: none;
    }
    .col-phone,
    .col-email {
      width: 50%;
    }
    .fa-sort-down {
      top: 40%;
    }

    .block-heading {
      display:flex;
      flex-direction:column;      
      align-items: center;
      justify-content: center;
    }
    .block-heading .heading-title {
      width: 100%;      
      font-size: 1.6rem;
      text-align:center;
    }
    .block-heading .heading-button {
      width: auto;      
      text-align:center;
    }
    .listing-items {
      margin-left: 0px;
    }
    
    .see-all .btn {
      -webkit-box-shadow: 2px 2px 5px 2px rgba(0, 0, 0, 0.21);
      -moz-box-shadow: 2px 2px 5px 2px rgba(0, 0, 0, 0.21);
      box-shadow: 2px 2px 5px 2px rgba(0, 0, 0, 0.21);
      background: #fff;
      color: #221e1f !important;
      font-size: 1.25rem;
      max-width: 177px !important;
      min-width: 177px !important;
      border-radius: 0;
      margin: 20px 10px;
    }
  }
`;
