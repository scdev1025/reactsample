import { Component } from "react";
import styles from "./styles";

class TopBar extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { agent, contactAgentAction, isContactAgent } = this.props;

    return (
      <div className="top-profile-detail-bar">
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-5">
              <h1>{agent.firstName + " " + agent.lastName}</h1>
              <div className="designation">{agent.title}</div>
            </div>
            <div className="col-md-7">
              <div className="top-profile-right-detail float-right">
                <div className="contact">
                  <div className="phone">
                    <span>T</span> {agent.phone}
                  </div>
                  <div className="fax float-right">
                    <span>F</span> {agent.fax}
                  </div>
                </div>
                <div className="contact-agent">
                  <a className="btn btn-default" onClick={contactAgentAction}>
                    {isContactAgent ? "Cancel" : "Contact agent"}
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default TopBar;
