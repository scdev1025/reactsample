import { Component } from "react";
import styles from "./styles";

class VerticalImageSlider extends Component {
  render() {
    const { imageList } = this.props;

    return (
      <div className="vertical-slider">
        {imageList.map((item, index) => (
          <div
            key={index}
            className="item img-background"
            style={{ backgroundImage: `url(${item})` }}
          >
            <div className={index == 0 ? "selected" : "unselected"} />
          </div>
        ))}
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default VerticalImageSlider;
