import { Component } from "react";
import ImageSlider from "./ImageSlider";
import styles from "./styles";
import { bedIcon, bathIcon, carIcon } from "../shared/variables";
import PropTypes from "prop-types";

class ListingItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { property } = this.props;

    return (
      <div className="listing-item">
        <div className="listing-slide">
          <ImageSlider />
        </div>
        <div className="latest-right-detail">
          <div className="heading">
            {property.title} <span className="address">{property.address}</span>
          </div>
          <div className="sub-heading">{property.subtitle}</div>
          <div className="content-agent-text">Contact Agent</div>
          <div className="property-icon">
            <ul>
              <li className="shadow">
                <a href="#">
                  <img src={bedIcon} alt="icon_bed" />
                  <span className="count">{property.bed_count}</span>
                </a>
              </li>
              <li className="shadow">
                <a href="#">
                  <img src={bathIcon} alt="icon_bath" />
                  <span className="count">{property.bath_count}</span>
                </a>
              </li>
              <li className="shadow">
                <a href="#">
                  <img src={carIcon} alt="icon_car" />
                  <span className="count">{property.car_count}</span>
                </a>
              </li>
            </ul>
          </div>
          <div className="latest-right-profile-pic">
            <img src={property.agent.profileImageURL} alt="img_profile" />
            <div className="latest-list-profile-pic-name">
              {property.agent.firstName + " " + property.agent.lastName}
            </div>
          </div>
        </div>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

ListingItem.defaultProps = {
  property: {
    title: "St Kilda",
    address: "23 Baker Street",
    subtitle: "A HOME WITH TRUE HEART, SPACE AND SOUL",
    bed_count: 4,
    bath_count: 2,
    car_count: 1,
    agent: {
      firstName: "Torsten",
      lastName: "Kasper",
      profileImageURL: "../../static/images/img_list_profile.png"
    }
  }
};

ListingItem.propTypes = {
  property: PropTypes.shape({
    title: PropTypes.string.isRequired,
    address: PropTypes.string.isRequired,
    subtitle: PropTypes.string.isRequired,
    bed_count: PropTypes.number.isRequired,
    bath_count: PropTypes.number.isRequired,
    car_count: PropTypes.number.isRequired,
    agent: PropTypes.shape({
      firstName: PropTypes.string.isRequired,
      lastName: PropTypes.string.isRequired,
      profileImageURL: PropTypes.string.isRequired
    })
  }).isRequired
};

export default ListingItem;
