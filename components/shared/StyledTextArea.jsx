import { Component } from "react";
import styles from "./styles";

class StyledTextArea extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="styled-input">
        <label>{this.props.title}</label>
        <textarea          
          type="key"
          className="form-control"
          id="search-key"
          placeholder={this.props.placeholder}
        />
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default StyledTextArea;
