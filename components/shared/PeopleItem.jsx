import styles from "./styles";
import PropTypes from "prop-types";

const PeopleItem = props => {
  const { agent } = props;
  return (
    <div className="people-item">
      <div className="profile-image">
        <img src={agent.profileImageURL} />
      </div>
      <div className="profile-info">
        <div className="profile-name">
          {agent.firstName + " " + agent.lastName}
        </div>
        <div className="profile-heading">{agent.title}</div>
        <div className="phone">
          Phone <div className="number">{agent.phone}</div>
        </div>
        <div className="profile-contact">
          <a href="#" className="sending-email">
            {"Email " + agent.firstName}
          </a>
          <span className="separator">|</span>
          <a href="#" className="view-profile">
            View Profile
          </a>
        </div>
      </div>
      <style jsx>{styles}</style>
    </div>
  );
};

PeopleItem.defaultProps = {
  agent: {
    firstName: "Torsten",
    lastName: "Kasper",
    title: "Managing Director",
    phone: "0404 040 040",
    profileImageURL: "../../../static/images/img_list_profile.png"
  }
};

PeopleItem.propTypes = {
  agent: PropTypes.shape({
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    phone: PropTypes.string.isRequired,
    profileImageURL: PropTypes.string.isRequired
  }).isRequired
};

export default PeopleItem;
