import { Component } from "react";
import { Carousel, CarouselItem, CarouselControl } from "reactstrap";
import PropTypes from "prop-types";

class Testimonial extends Component {
  constructor(props) {
    super(props);
    this.state = { activeIndex: 0 };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
  }

  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next() {
    if (this.animating) return;
    const nextIndex =
      this.state.activeIndex === this.props.itemList.length - 1
        ? 0
        : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous() {
    if (this.animating) return;
    const nextIndex =
      this.state.activeIndex === 0
        ? this.props.itemList.length - 1
        : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  }

  render() {
    const { activeIndex } = this.state;
    const { itemList } = this.props;

    const slider = itemList.map((item, index) => {
      return (
        <CarouselItem
          onExiting={this.onExiting}
          onExited={this.onExited}
          key={index}
        >
          <div className="testimonial-content d-flex flex-column justify-content-center align-items-center text-center">
            <div className="title black">{item.headingText}</div>
            <div className="font-italic black-content-text">
              {item.bodyText}
              <a href="#" className="orange">
                &nbsp;read more
              </a>
            </div>
            <div className="content-title orange">{item.name}</div>
          </div>
        </CarouselItem>
      );
    });

    return (
      <Carousel
        activeIndex={activeIndex}
        next={this.next}
        previous={this.previous}
      >
        {/* <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={this.goToIndex} /> */}
        {slider}
        <CarouselControl
          className="shadow"
          direction="prev"
          directionText="Previous"
          onClickHandler={this.previous}
        />
        <CarouselControl
          className="shadow"
          direction="next"
          directionText="Next"
          onClickHandler={this.next}
        />
      </Carousel>
    );
  }
}

Testimonial.defaultProps = {
  itemList: [
    {
      headingText: "A WORD FROM OUR FRIENDS",
      bodyText:
        "We found Torsten and the team to be honest, dedicated and direct and of great integrity...",
      linkText: "abc.com",
      readmoreLink: "",
      name: "Will & Claire Gourlay"
    },
    {
      headingText: "A WORD FROM OUR FRIENDS",
      bodyText:
        "We found Torsten and the team to be honest, dedicated and direct and of great integrity...",
      linkText: "abc.com",
      readmoreLink: "",
      name: "Will & Claire Gourlay"
    },
    {
      headingText: "A WORD FROM OUR FRIENDS",
      bodyText:
        "We found Torsten and the team to be honest, dedicated and direct and of great integrity...",
      linkText: "abc.com",
      readmoreLink: "",
      name: "Will & Claire Gourlay"
    }
  ]
};

Testimonial.propTypes = {
  itemList: PropTypes.arrayOf(
    PropTypes.shape({
      headingText: PropTypes.string.isRequired,
      bodyText: PropTypes.string.isRequired,
      linkText: PropTypes.string.isRequired,
      readmoreLink: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired
    })
  ).isRequired
};

export default Testimonial;
