import css from "styled-jsx/css";

export default css`
  .get-touch h2 {
    color: #ff8124;
    font-size: 2.5rem;
    margin: -4px 0 1.5625rem 0;
  }
  .get-touch h2,
  .get-touch .btn,
  .contact-agent .btn,
  .see-all btn,
  .styled-select select {
    font-weight: 500;
    font-family: "museo", serif;
  }
  .get-touch .form-group {
    margin-bottom: 24px;
  }
  .get-touch .btn {
    font-size: 1.5rem;
    -moz-box-shadow: 1px 2px 3px 1px rgba(0, 0, 0, 0.31);
    box-shadow: 1px 2px 3px 1px rgba(0, 0, 0, 0.31);
    border-radius: 0;
    color: #fff;
    background: #ff8124;
    padding: 0.625rem 0 !important;
    text-align: center;
    width: 100%;
  }
  .get-touch .btn:hover {
    background: #ee7a24;
  }
  .get-touch .form-control {
    -webkit-box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.31);
    -moz-box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.31);
    box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.21);
    border-radius: 0;
    height: 68px;
    font-size: 1.25rem;
    font-family: "museo", serif;
    border: none;
  }
  .get-touch .form-control {
    padding: 0.125rem 1.3125rem 2.8125rem 1.3125rem;
  }

  .styled-select {
    border: none;
    background: white;
    box-sizing: border-box;
    border-radius: 3px;
    padding: 0.375rem 1rem;
    overflow: hidden;
    position: relative;
    -webkit-box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.31);
    -moz-box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.31);
    box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.21);
  }
  .styled-select label {
    font-size: 1rem;
    font-family: "museo", serif;
    font-weight: 400;
    color: #ff8124;
    margin-bottom: 0;
  }
  .styled-select,
  .styled-select select {
    width: 100%;
  }

  .styled-select select {
    width: 100%;
    font-size: 1.75rem;
    color: #221e1f;
    margin-top: -5px;
  }
  select:focus {
    outline: none;
  }
  .styled-select select {
    padding: 0;
    background: transparent;
    border: none;
    margin-top: -5px;
    -webkit-appearance: none;
  }
  .styled-select .fa-sort-down {
    position: absolute;
    top: 45%;
    right: 16px;
    font-size: 17px;
    color: #ff8124;
  }

  .listing-item {
    margin-bottom: 2.1875rem;
    height: 220px;
    width: 100%;
    display: flex;
    flex-direction: row;
  }

  .latest-right-profile-pic {
    position: absolute;
    right: 0;
    top: 20px;
    max-height: 200px;    
    overflow: hidden;
  }

  .latest-list-profile-pic-name {
    font-size: 1rem;
    color: #fff;
    width: 164px;
    height: 27px;
    text-align: center;
    background: #ff8124;
    position: absolute;
    bottom: 38px;
    right: 0;
    line-height: 26px;
  }

  .content-agent-text {
    font-style: italic;
    margin-bottom: 40px;
    color: #221e1f !important;
    font-size: 1.2rem;
  }

  .latest-right-detail .sub-heading {
    font-size: 1rem;
    margin-bottom: 0.75rem;
    font-weight: 500;
    color: #ff8124;
  }

  .latest-right-detail {
    -webkit-box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.31);
    -moz-box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.31);
    box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.21);
    background: #fff;
    padding: 20px;
    height: 100%;
    width: 65%;
    position: relative;
  }
  .latest-right-detail .heading,
  .sub-heading,
  .content-agent-text,
  .travel-icon {
    width: 100%;
  }

  .latest-right-detail .heading {
    font-size: 1.5rem;
    color: #ff8124;
    font-weight: 500;
    margin-bottom: 1.1875rem;
  }
  .latest-right-detail .heading .address {
    color: #221e1f !important;
    font-size: 1rem;
  }
  .listing-slide {
    width: 35%;
    height: 100%;
  }

  .readmore {
    padding: 10px;
  }
  .readmore-downarrow {
    margin-left: 10px;
    transform: translateY(-2px);
  }

  .people-item {
    background: white;
    -webkit-box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.31);
    -moz-box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.31);
    box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.21);
    display: flex;
    flex-direction: row;
    height: 180px;
    margin-bottom: 20px;
  }
  .people-item .profile-image {
    overflow: hidden;
    width: 30%;
  }
  .people-item .profile-image img {
    resizemode: "contain";
  }
  .people-item .profile-info {
    width: 70%;
    display: flex;
    flex-direction: column;
    padding-bottom: 10px;
  }
  .people-item .profile-info .profile-name {
    background: #ff8214;
    color: white;
    font-size: 1.25rem;
    font-weight: 500;
    padding: 5px 10px;
  }
  .people-item .profile-info .profile-heading {
    font-weight: 500;
    font-size: 1.1rem;
    padding-left: 10px;
    padding-top: 10px;
    flex: 1;
  }
  .people-item .profile-info .phone {
    font-size: 1.1rem;
    padding-left: 10px;
    color: #ff8214;
    display: flex;
    flex-direction: row;
  }
  .people-item .profile-info .phone .number {
    color: black;
    margin-left: 10px;
  }
  .people-item .profile-info .profile-contact {
    padding-left: 10px;
  }
  .people-item .profile-info .profile-contact,
  .people-item .profile-info .profile-contact a {
    font-size: 1.1rem;
    color: #ff8214;
  }
  .people-item .profile-info .profile-contact .separator {
    margin: 0px 10px;
  }

  .styled-input {
    width: 100%;
    background: white;
    border: none;
    box-sizing: border-box;
    border-radius: 3px;
    padding: 0.375rem 1.3125rem;
    overflow: hidden;
    position: relative;
    -webkit-box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.31);
    -moz-box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.31);
    box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.21);
  }
  .styled-input label {
    font-size: 16px;
    font-family: "museo", serif;
    font-weight: 400;
    color: #ff8124;
    margin-bottom: 0;
  }
  .styled-input input {
    border: none;
    font-size: 1.75rem;
    color: black;
  }
  .styled-input input:focus {
    border-color: inherit;
    -webkit-box-shadow: none;
    box-shadow: none;
  }
  .styled-input textarea {
    border: none;
    font-size: 1.75rem;
    color: black;
    height: 125px;
  }
  .styled-input textarea:focus {
    border-color: inherit;
    -webkit-box-shadow: none;
    box-shadow: none;
  }

  .pagenation {
    margin-bottom: 50px;
  }
  .pagenation .btn {
    border: none;
    border-radius: 0;
    margin: 5px;
    background: #ff8214;
    padding: 10px 20px;
    font-size: 1.25rem;
  }
  .pagenation .number-btn {
    color: black;
    background: white;
  }
  .pagenation .selected {
    color: #9f9d9e;
    background: #f7f7f7;
  }

  .vertical-slider {
    height: 100%;
    display: flex;
    flex-direction: column;
  }
  .vertical-slider .item {
    height: 33.33%;
  }
  .vertical-slider .item .selected {
    height: 100%;
    border: 1px solid white;
  }
  .vertical-slider .item .unselected {
    background: rgba(0, 0, 0, 0.5);
    height: 100%;
  }

  .property-map-item {
    height: 100px;
    width: 350px;
    display: flex;
    flex-direction: row;
    -webkit-box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.31);
    -moz-box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.31);
    box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.21);
  }
  .property-map-item .img-background {
    width: 40%;
    height: 100%;
  }
  .property-map-item .detail-info {
    background: #fff;
    padding: 10px 10px;
    height: 100%;
    width: 60%;
    position: relative;
  }
  .property-map-item .detail-info .heading {
    color: #ff8214;
    font-size: 1.25rem;
    font-weight: 500;
  }
  .property-map-item .detail-info .address {
    font-size: 1rem;
  }
  .property-map-item .detail-info .content-agent {
    font-style: italic;
    font-size: 1rem;
    margin-top: 5px;
  }
  .property-map-item .detail-info .delete-button {
    width: 20px;
    position: absolute;
    top: 0;
    right: 0;
    transform: translate(10px, -10px);
  }
  .property-map-item .property-icon {
    width: auto;
    transform: translate(-10%, -50%) scale(0.6);
  }

  .mapview-item {
    display: inline-block;    
  }
  .mapview-item .heading {
    display: inline-block;
    background: #ff8214;
    color: white;
    font-size: 1.25rem;
    font-weight: 500;
    padding: 0px 5px;
    -webkit-box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.31);
    -moz-box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.31);
    box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.21);
  }
  .mapview-item .heading.school {
    background: white;
    color: #ff8214;
  }
  .mapview-item .arrow-down {
    margin-top: -5px;
  }
  .mapview-item .arrow-down.school {
    z-index: -1;
  }
  .mapview-item .item-icon {
    width: 30px;
    margin-top: 5px;
  }

  .contact-agent-form {
    background: #221e1f;
    height: 100vh;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: flex-start;
    padding-top: 50px;
    margin-left: 0px;
    margin-right: 0px;
  }
  .contact-agent-form .first-column {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    height: 250px;
  }
  .contact-agent-form .second-column {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    height: 250px;
  }
  .contact-agent-form .second-column .btn-contact {
    background: #ff8214;
    width: 100%;
    height: 30%;
  }

  .sortby-select {
    min-width: 120px;
    margin: 5px 10px;
    padding: 10px;
  }
  .sortby-select select {
    color: #ff8124;
    background: transparent;
    border: none;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    width: 100%;
    font-size: 1.5rem;
  }
  .sortby-select select:focus {
    outline: none;
  }
  .sortby-select span {
    position: absolute;
    transform: translate(50px, -5px);
  }
  @media screen and (max-width: 767px) {
    .styled-select select,
    .top-profile-detail-bar h1 {
      font-size: 1.5rem;
    }
    .get-touch textarea {
      min-height: 90px;
      max-height: 90px;
    }
    .get-touch .col-email {
      width: 50%;
      padding-right: 5px;
    }
    .get-touch .col-phone {
      width: 50%;
      padding-left: 5px;
    }
    .listing-item {
      height: auto;
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
    }
    .latest-right-detail {
      -webkit-box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.31);
      -moz-box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.31);
      box-shadow: 2px 2px 3px 1px rgba(0, 0, 0, 0.21);
      background: #fff;
      padding: 0px;
      height: 300px;
      width: 100%;
    }
    .latest-right-detail .heading {
      font-size: 1.75rem;
      color: #ff8124;
      font-weight: 500;
      margin-bottom: 1.1875rem;
      margin: 10px;
    }
    .latest-right-detail .heading .address {
      color: #221e1f !important;
      font-size: 1.25rem;
      margin-right: 20px;
      float: right;
    }
    .content-agent-text {
      font-style: italic;
      margin-bottom: 50px;
      margin-left: 10px;
      color: #221e1f !important;
      font-size: 1.25rem;
      width: 60%;
    }

    .latest-right-detail .sub-heading {
      font-size: 1.25rem;
      margin-bottom: 0.75rem;
      font-weight: 500;
      color: #ff8124;
      margin-left: 10px;
      width: 60%;
    }
    .latest-right-profile-pic {
      position: absolute;
      right: 0px;
      top: 50px;
      max-height: 250px;
      overflow: hidden;
    }
    .listing-slide {
      width: 100%;
      height: auto;
    }

    .people-item {
      height: 250px;
    }
    .people-item .profile-image {
      display:flex;      
      align-items: flex-end;
    }    
    .people-item .profile-info .profile-name {
      font-size: 1.5rem;
    }
    .people-item .profile-info .profile-heading {     
      font-size: 1.25rem;
    }
    .people-item .profile-info .profile-contact {
      display: flex;
      flex-direction: column;
      font-size: 1.25rem;
    }
    .people-item .profile-info .phone {
      display: flex;
      flex-direction: column;
      font-size: 1.25rem;
    }
    .people-item .profile-info .phone .number {
      margin-left: 0px;
      margin-bottom: 10px;
      font-size: 1.25rem;
    }
    .people-item .profile-info .profile-contact a {
      width: 100%;
      font-size: 1.25rem;
    }
    .people-item .profile-info .profile-contact .separator {
      display: none;
    }

    .styled-select {
      height: 70px;
      margin: 5px 0px;
    }
    .styled-select select {
      margin: 0px;
      width: 100%;
      font-size: 1.5rem;
      height: 30px;
      padding: 0;
      background: transparent;
      border: none;
      -webkit-appearance: none;
    }

    .pagenation {
      margin-bottom: 0px;
    }
    .pagenation .btn {
      border: none;
      border-radius: 0;
      margin: 3px;
      background: #ff8214;
      padding: 5px 7px;
      font-size: 1rem;
    }
    .pagenation .number-btn {
      color: black;
      background: white;
    }
    .pagenation .selected {
      color: #9f9d9e;
      background: #f7f7f7;
    }

    .vertical-slider {
      display: flex;
      flex-direction: row;
    }
    .vertical-slider .item {
      height: 100%;
      width: 33.33%;
    }

    .contact-agent-form {
      background: #221e1f;
      height: 550px;
      display: flex;
      flex-direction: row;
      justify-content: center;
      align-items: flex-start;
      padding-top: 20px;
      padding-left: 10px;
      padding-right: 10px;
    }
    .contact-agent-form .first-column {
      display: flex;
      flex-direction: column;
      justify-content: space-between;
      height: 250px;
    }
    .contact-agent-form .second-column {
      display: flex;
      flex-direction: column;
      justify-content: space-between;
      height: 250px;
    }
    .contact-agent-form .second-column .btn-contact {
      background: #ff8214;
      width: 100%;
      height: 25%;
    }
  }
`;
