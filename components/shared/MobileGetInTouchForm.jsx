import { Component } from "react";
import styles from "./styles";

class MobileGetInTouchForm extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <form action="" className="get-touch">
        <div className="form-group">
          <div className="row clearfix">
            <div className="col-md-12">
              <input
                type="name"
                className="form-control"
                id="email"
                placeholder="Name"
              />
            </div>
          </div>
        </div>
        <div className="form-group">
          <div className="row clearfix">
            <div className="col-md-6 col-xs-6 col-sm-6 col-email">
              <input
                type="email"
                className="form-control"
                id="email"
                placeholder="Email"
              />
            </div>
            <div className="col-md-6 col-xs-6 col-sm-6 col-phone">
              <input
                type="Phone Number"
                className="form-control"
                id="Phonenumber"
                placeholder="Phone"
              />
            </div>
          </div>
        </div>

        <div className="form-group">
          <div className="row clearfix">
            <div className="col-md-12">
              <textarea
                type="Message"
                className="form-control"
                id="Message"
                placeholder="Message"
                rows="5"
              />
            </div>
          </div>
        </div>

        <div className="form-group">
          <div className="row clearfix">
            <div className="col-md-12">
              <div className="styled-select">
                <label>What can we help with?</label>
                <select>
                  <option>Property appraisal</option>
                  <option>Property appraisal</option>
                  <option>Property appraisal</option>
                </select>
                <span className="fas fa-sort-down" />
              </div>
            </div>
          </div>
        </div>
        <button type="submit" className="btn btn-default" title="Submit">
          Submit
        </button>
        <style jsx>{styles}</style>
      </form>
    );
  }
}

export default MobileGetInTouchForm;
