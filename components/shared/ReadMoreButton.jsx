import styles from "./styles";

const ReadMoreButton = () => {
  return (
    <div className="readmore text-center">
      <a href="#" className="orange min-small-size medium-font-weight">
        Read More
        <span className="readmore-downarrow fas fa-sort-down orange small-size" />
      </a>
      <style jsx>{styles}</style>
    </div>
  );
};

export default ReadMoreButton;
