import { Component } from "react";
import StyledInput from "./StyledInput";
import StyledTextArea from "./StyledTextArea";
import styles from "./styles";

class ContactAgentForm extends Component {
  render() {    

    return (
      <div className="contact-agent-form row">
        <div className="first-column col-md-4">
          <StyledInput title="Name" placeholder="" />
          <StyledInput title="Email" placeholder="" />
          <StyledInput title="Phone" placeholder="" />
        </div>
        <div className="second-column col-md-4">
          <StyledTextArea title="Message" placeholder="" />
          <button type="button" className="btn btn-primary btn-contact shadow">
            Contact the agent
          </button>
        </div>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default ContactAgentForm;
