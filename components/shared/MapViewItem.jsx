import { Component } from "react";
import styles from "./styles";
import { houseIcon, arrowDownIcon, schoolIcon } from "../shared/variables";
import PropTypes from "prop-types";

class MapViewItem extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { title, isHome } = this.props;

    return (
      <div className="mapview-item">
        <div className={ isHome ? "heading" : "heading school" }>{title}</div>
        <div className="flex-row-center"><img className={ isHome ? "arrow-down" : "arrow-down school"} src={arrowDownIcon} alt={arrowDownIcon} /></div>
        <div className="flex-row-center"><img className="item-icon" src={isHome? houseIcon : schoolIcon} alt="item-icon" /></div>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

MapViewItem.defaultProps = {
  title: "23 Baker St, St Kilda",
  isHome: true
};

MapViewItem.propTypes = {
  property: PropTypes.shape({
    title: PropTypes.string.isRequired,
    isHome: PropTypes.bool.isRequired
  }).isRequired
};

export default MapViewItem;
