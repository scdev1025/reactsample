import { Component } from "react";
import { Carousel, CarouselItem, CarouselControl } from "reactstrap";
import PropTypes from "prop-types";

class ImageSlider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeIndex: 0,
    };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
  }
  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next() {
    if (this.animating) return;
    const nextIndex =
      this.state.activeIndex === this.props.imageList.length - 1
        ? 0
        : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous() {
    if (this.animating) return;
    const prevIndex =
      this.state.activeIndex === 0
        ? this.props.imageList.length - 1
        : this.state.activeIndex - 1;
    this.setState({ activeIndex: prevIndex });
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  }
  render() {
    const { activeIndex } = this.state;
    const {imageList} = this.props;

    const slides = imageList.map((item, index) => {
      return (
        <CarouselItem
          onExiting={this.onExiting}
          onExited={this.onExited}
          key={index}
        >
          <img
            src={item}
            alt={item}
            className="img-responsive"
          />

          {/* <div className="img-background" style={{ backgroundImage: `url(${item})` }} />    */}
        </CarouselItem>
      );
    });

    return (
      <Carousel
        activeIndex={activeIndex}
        next={this.next}
        previous={this.previous}
      >
        {/* <CarouselIndicators items={this.state.saleItems} activeIndex={activeIndex} onClickHandler={this.goToIndex} /> */}
        {slides}
        <CarouselControl
          direction="prev"
          directionText="Previous"
          onClickHandler={this.previous}
        />
        <CarouselControl
          direction="next"
          directionText="Next"
          onClickHandler={this.next}
        />
      </Carousel>
    );
  }
}

ImageSlider.defaultProps = {
  imageList: [
    "../../static/images/home/img_property_sales.png",
    "../../static/images/home/img_property_rental.png",
    "../../static/images/home/img_property_sales.png",
    "../../static/images/home/img_property_rental.png",
  ]
};

ImageSlider.propTypes = {
  imageList: PropTypes.arrayOf(PropTypes.string).isRequired
};

export default ImageSlider;
