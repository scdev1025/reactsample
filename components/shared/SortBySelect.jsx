import { Component } from "react";
import styles from "./styles";

class SortBySelect extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="sortby-select flex-column-center">
        <select className="text-center">
          <option>Sort by</option>
          <option>Sort by</option>
          <option>Sort by</option>
        </select>
        <span className="fas fa-sort-down orange small-size" />
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default SortBySelect;
