import { Component } from "react";
import styles from "./styles";
import { bedIcon, bathIcon, carIcon, deleteIcon } from "../shared/variables";
import PropTypes from "prop-types";

class PropertyMapItem extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { property } = this.props;

    return (
      <div className="property-map-item">
        <div
          className="img-background"
          style={{ backgroundImage: `url(${property.imageURL})` }}
        >
          <div className="property-icon">
            <ul>
              <li className="shadow">
                <a href="#">
                  <img src={bedIcon} alt="icon_bed" />
                  <span className="count">{property.bed_count}</span>
                </a>
              </li>
              <li className="shadow">
                <a href="#">
                  <img src={bathIcon} alt="icon_bath" />
                  <span className="count">{property.bath_count}</span>
                </a>
              </li>
              <li className="shadow">
                <a href="#">
                  <img src={carIcon} alt="icon_car" />
                  <span className="count">{property.car_count}</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div className="detail-info">
          <div className="heading">{property.title}</div>
          <div className="address">{property.address}</div>
          <div className="content-agent">Contact Agent</div>
          <a>
            <img className="delete-button" src={deleteIcon} alt={deleteIcon} />
          </a>
        </div>
        <style jsx>{styles}</style>
      </div>
    );
  }
}

PropertyMapItem.defaultProps = {
  property: {
    title: "St Kilda",
    address: "23 Baker Street",
    subtitle: "A HOME WITH TRUE HEART, SPACE AND SOUL",
    bed_count: 1,
    bath_count: 1,
    car_count: 1,
    imageURL: "../../static/images/home/img_property_sales.png"
  }
};

PropertyMapItem.propTypes = {
  property: PropTypes.shape({
    title: PropTypes.string.isRequired,
    address: PropTypes.string.isRequired,
    subtitle: PropTypes.string.isRequired,
    bed_count: PropTypes.number.isRequired,
    bath_count: PropTypes.number.isRequired,
    car_count: PropTypes.number.isRequired,
    imageURL: PropTypes.string.isRequired
  }).isRequired
};

export default PropertyMapItem;
