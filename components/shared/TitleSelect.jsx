import { Component } from "react";
import styles from "./styles";

class TitleSelect extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="styled-select">
        <label>{this.props.title}</label>
        <select>
          <option>{this.props.list}</option>
          <option>{this.props.list}</option>
          <option>{this.props.list}</option>
        </select>
        <span className="fas fa-sort-down" />
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default TitleSelect;
