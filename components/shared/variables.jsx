module.exports = Object.freeze({
  facebookIcon: "../../static/images/icon_social_facebook.png",
  twitterIcon: "../../static/images/icon_social_twitter.png",
  instagramIcon: "../../static/images/icon_social_instagram.png",
  pinterestIcon: "../../static/images/icon_social_pinterest.png",
  blogIcon: "../../static/images/icon_social_blog.png",
  bedIcon: "../../static/images/icon_property_bed.png",
  bathIcon: "../../static/images/icon_property_bath.png",
  carIcon: "../../static/images/icon_property_car.png",
  LogoIcon: "../static/images/img_logo.png",
  whiteLogo_2x: "../../static/images/img_logo_white_2x.png",
  whiteLogo: "../../static/images/img_logo_white.png",
  houseIcon: "../static/images/icon_house.png",
  personIcon: "../static/images/icon_person.png",
  markerIcon: "../static/images/icon_marker.png",
  tickIcon: "../static/images/properties/icon_tick.png",
  calendarIcon: "../static/images/properties/icon_calendar.png",
  arrowDownIcon: "../static/images/icon_arrow_down.png",
  playRoundIcon: "../static/images/img_play_round.png",
  schoolIcon: "../static/images/icon_school.png",

  sellIcon: "../static/images/mobile/icon_mobile_menu_sell.png",
  buyIcon: "../static/images/mobile/icon_mobile_menu_buy.png",
  leaseIcon: "../static/images/mobile/icon_mobile_menu_lease.png",
  rentIcon: "../static/images/mobile/icon_mobile_menu_rent.png",
  moreIcon: "../static/images/mobile/icon_mobile_menu_more.png",
  deleteIcon: "../static/images/icon_delete.png",  
});
