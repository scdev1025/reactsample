import { Component } from "react";
import styles from "./styles";

class StyledInput extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="styled-input">
        <label>{this.props.title}</label>
        <input
          type="key"
          className="form-control"
          id="search-key"
          placeholder={this.props.placeholder}
          value={this.props.text}
        />
        <style jsx>{styles}</style>
      </div>
    );
  }
}

export default StyledInput;
