import { Component } from "react";
import styles from "./styles";

class Pagenation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: this.props.currentPage,
      pageCount: this.props.pageCount
    };

    this.gotoNextPage = this.gotoNextPage.bind(this);
    this.gotoLastPage = this.gotoLastPage.bind(this);
    this.gotoPrevPage = this.gotoPrevPage.bind(this);
    this.gotoFirstPage = this.gotoFirstPage.bind(this);
    this.gotoPage = this.gotoPage.bind(this);
  }

  gotoNextPage() {
    const { currentPage, pageCount } = this.state;
    var nextPage = currentPage + 1;
    if (nextPage > pageCount) {
      nextPage = pageCount;
    }

    this.setState({
      currentPage: nextPage
    });
  }

  gotoLastPage() {
    const { pageCount } = this.state;
    this.setState({
      currentPage: pageCount
    });
  }

  gotoPage(pageNumber) {
    this.setState({
      currentPage: pageNumber
    });
  }

  gotoPrevPage() {
    const { currentPage } = this.state;
    var prevPage = currentPage - 1;
    if (prevPage < 1) {
      prevPage = 1;
    }

    this.setState({
      currentPage: prevPage
    });
  }

  gotoFirstPage() {
    this.setState({
      currentPage: 1
    });
  }

  render() {
    const { pageCount, currentPage } = this.state;
    const displayPageCount = 5;

    return (
      <div className="pagenation flex-row-center">
        {currentPage > 1
          ? [
              <button
                type="button"
                className="btn control-btn btn-primary shadow"
                onClick={() => this.gotoFirstPage()}
              >
                First
              </button>,
              <button
                type="button"
                className="btn control-btn btn-primary shadow"
                onClick={() => this.gotoPrevPage()}
              >
                Prev
              </button>
            ]
          : null}

        {pageCount <= displayPageCount
          ? Array(pageCount)
              .fill(0)
              .map((item, index) => {
                return (
                  <button
                    key={index}
                    type="button"
                    className={
                      currentPage == index + 1
                        ? "btn number-btn btn-primary shadow selected"
                        : "btn number-btn btn-primary shadow"
                    }
                    onClick={() => this.gotoPage(index + 1)}
                  >
                    {index + 1}
                  </button>
                );
              })
          : Array(pageCount)
              .fill(0)
              .map((item, index) => {
                var pageNum = index + 1;
                if (index > 0 && index < pageCount - 1) {
                  if (
                    (pageNum + 1 == currentPage && index != 0) ||
                    (pageNum - 1 == currentPage && pageNum != pageCount)
                  ) {
                    return (
                      <button
                        key={index}
                        type="button"
                        className="btn number-btn btn-primary shadow"
                      >
                        ...
                      </button>
                    );
                  }
                  if (pageNum == currentPage) {
                    return (
                      <button
                        key={index}
                        type="button"
                        className={
                          currentPage == pageNum
                            ? "btn number-btn btn-primary shadow selected"
                            : "btn number-btn btn-primary shadow"
                        }
                        onClick={() => this.gotoPage(pageNum)}
                      >
                        {pageNum}
                      </button>
                    );
                  } else {
                    return null;
                  }
                }

                return (
                  <button
                    key={index}
                    type="button"
                    className={
                      currentPage == pageNum
                        ? "btn number-btn btn-primary shadow selected"
                        : "btn number-btn btn-primary shadow"
                    }
                    onClick={() => this.gotoPage(pageNum)}
                  >
                    {index + 1}
                  </button>
                );
              })}

        {currentPage < pageCount
          ? [
              <button
                type="button"
                className="btn control-btn btn-primary shadow"
                onClick={() => this.gotoNextPage()}
              >
                Next
              </button>,
              <button
                type="button"
                className="btn control-btn btn-primary shadow"
                onClick={() => this.gotoLastPage()}
              >
                Last
              </button>
            ]
          : null}

        <style jsx>{styles}</style>
      </div>
    );
  }
}

Pagenation.defaultProps = {
  currentPage: 1,
  pageCount: 1
};
export default Pagenation;
