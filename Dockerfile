FROM node:alpine
# Create app directory
RUN mkdir -p /code
WORKDIR /code
# Install app dependencies
COPY package.json /code
RUN npm install
# Bundle app source
COPY . /code
EXPOSE 3000
