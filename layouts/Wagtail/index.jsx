import ContentPage from './ContentPage'
import AgentIndexPage from './AgentIndexPage'

export default {
  ContentPage: ContentPage,
  AgentIndexPage: AgentIndexPage
};