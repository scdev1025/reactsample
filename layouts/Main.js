import Meta from "../components/global/Meta";
import Header from "../components/global/Header";
import Footer from "../components/global/Footer";
import MobileMenu from "../components/global/MobileMenu";

export default ({ children }) => (
  <div>
    <Meta />
    <Header />
    <section id="main" role="main">
      {children}
    </section>
    <Footer />
    <MobileMenu />
    <style jsx>{`
      #main {
        // padding: 2rem 0 3rem;
      }
    `}</style>
  </div>
);
