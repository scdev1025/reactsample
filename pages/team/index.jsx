import Page from "../../layouts/Main";
import styles from "./styles";
import Pagenation from "../../components/shared/Pagenation";
import SearchContainer from "../../components/team/SearchContainer";
import PropTypes from "prop-types";
import PeopleItem from "../../components/shared/PeopleItem";
import CheckOffice from "../../components/team/CheckOffice";

const TeamSearch = props => (
  <Page>
    <content>
      <div className="team-search-container flex-column-center">
        <div className="team-search-header container-fluid">
          <div className="heading text-center orange">MEET THE TEAM</div>
          <div className="sub-heading text-center black">
            We're right around the corner
          </div>
          <SearchContainer agentCount={props.agentCount} />
        </div>
        <div className="list row">
          <div className="col-md-4">
            <PeopleItem />
            <PeopleItem />
            <PeopleItem />
            <PeopleItem />
            <PeopleItem />
            <PeopleItem />
            <PeopleItem />
            <PeopleItem />
          </div>
          <div className="col-md-4">
            <PeopleItem />
            <PeopleItem />
            <PeopleItem />
            <PeopleItem />
            <PeopleItem />
            <PeopleItem />
            <PeopleItem />
            <PeopleItem />
          </div>
          <div className="col-md-4">
            <PeopleItem />
            <PeopleItem />
            <PeopleItem />
            <PeopleItem />
            <PeopleItem />
            <PeopleItem />
            <PeopleItem />
            <PeopleItem />
          </div>
          <div className="col-md-12">
            <Pagenation pageCount={7} currentPage={1} />
          </div>
        </div>
      </div>
      <CheckOffice />
    </content>
    <mcontent>
      <div className="team-search-container">
        <div className="team-search-header">
          <div className="heading text-center orange">MEET THE TEAM</div>
          <div className="sub-heading text-center black">
            We're right around the corner
          </div>
          <SearchContainer agentCount={props.agentCount} />
        </div>
        <div className="list">
          <PeopleItem />
          <PeopleItem />
          <PeopleItem />
          <PeopleItem />
          <PeopleItem />
        </div>
        <Pagenation pageCount={7} currentPage={1} />
      </div>
      <CheckOffice />
    </mcontent>
    <style jsx>{styles}</style>
  </Page>
);

TeamSearch.defaultProps = {
  agentCount: 24
};

TeamSearch.propTypes = {
  agentCount: PropTypes.number.isRequired
};

export default TeamSearch;
