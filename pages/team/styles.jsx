import css from "styled-jsx/css";

export default css`
  content {
    display: inherit;
  }
  mcontent {
    display: none;
  }

  content {
    padding-top: 90px;
  }

  .team-search-header {
    padding: 30px;
    padding-bottom: 10px;
  }

  .team-search-header .heading {
    font-size: 2rem;
    font-weight: 500;
  }

  .team-search-header .sub-heading {
    font-size: 1.5rem;
    font-weight: 500;
    margin-bottom: 20px;
  }

  .team-search-container {
    background: #efefef;
  }

  .list {
    padding: 0px 10px;
  }

  @media screen and (max-width: 767px) {
    content {
      display: none;
    }
    mcontent {
      display: inherit;
    }

    .team-search-container {
      padding-bottom: 20px;
    }
    .team-search-header {
      padding: 20px 0px;
    }
    .team-search-header .heading {
      font-size: 2rem;
    }
    .team-search-header .sub-heading {
      font-size: 1.25rem;
    }

    .list {
      padding: 0px;
    }
  }
`;
