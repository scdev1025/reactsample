import React from "react";
import "isomorphic-unfetch";
import WagtailPages from "../../layouts/Wagtail";
import Page from "../../layouts/Main";

class WagtailPage extends React.Component {
  constructor(props) {
    super(props);
  }

  static async getInitialProps({ query }) {
    let http_method = "http";
    let hostname = "chisholm-api.techequipt.com.au";
    const WAGTAIL_API_BASE = `${http_method}://${hostname}/api/cms/pages/`;
    const WAGTAIL_SLUG_FIND = `${WAGTAIL_API_BASE}find/?html_path=${
      query.slug
    }`;
    const response = await fetch(WAGTAIL_SLUG_FIND);
    const pageData = await response.json();
    const wagtailPageType = pageData.meta.type.split(".")[1];
    return { pageData, wagtailPageType };
  }

  render() {
    const { pageData, wagtailPageType } = this.props;
    const PageComponent = WagtailPages[wagtailPageType] || Page;
    return <PageComponent pageData={pageData} />;
  }
}

export default WagtailPage;
