import Page from "../../layouts/Main";
import OfficeCard from "../../components/offices/OfficeCard";
import MobileOfficeCard from "../../components/offices/MobileOfficeCard";
import PropTypes from "prop-types";
import styles from "./styles";

const OfficesPage = props => (
  <Page>
    <content>
      <div className="offices flex-column-center">
        <div className="offices-title orange medium-large-size medium-font-weight">
          OUR OFFICES
        </div>
        <div className="offices-subtitle black small-medium-size">
          We're right around the corner
        </div>
        <div className="offices-content flex-column-center">
          {props.officeList.map(office => (
            <OfficeCard key={office.title} office={office} />
          ))}
        </div>
      </div>
    </content>
    <mcontent>
      <div className="offices flex-column-center">
        <div className="offices-title orange medium-size medium-font-weight">
          OUR OFFICES
        </div>
        <div className="offices-subtitle black small-size">
          We're right around the corner
        </div>
        <div className="offices-content flex-column-center">
          {props.officeList.map(office => (
            <MobileOfficeCard key={office.title} office={office} />
          ))}
        </div>
      </div>
    </mcontent>
    <style jsx>{styles}</style>
  </Page>
);

OfficesPage.defaultProps = {
  officeList: [
    {
      title: "Elwood",
      address: "90 Ormond Road, Elwood Vic 3184",
      phone: "03 9531 1245",
      image: "./static/images/offices/img_office_elwood.png"
    },
    {
      title: "Port Melboume",
      address: "1/103D Bay Street, Port Melboumne Vic 3207",
      phone: "03 9646 4444",
      image: "./static/images/offices/img_office_portmelboume.png"
    },
    {
      title: "Black Rock",
      address: "3 Bluff Road, Black Rock Vic 3193",
      phone: "03 9589 3133",
      image: "./static/images/offices/img_office_blackrock.png"
    }
  ]
};

OfficesPage.propTypes = {
  officeList: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      address: PropTypes.string.isRequired,
      phone: PropTypes.string.isRequired,
      image: PropTypes.string.isRequired
    })
  ).isRequired
};

export default OfficesPage;
