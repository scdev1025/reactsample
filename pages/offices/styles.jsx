import css from "styled-jsx/css";

export default css`
  content {
    display: inherit;
  }
  mcontent {
    display: none;
  }

  .offices {
    padding-top: 150px;
    background: #efefef;
  }
  .offices-content {
    padding: 30px;
    width: 100%;
  }

  @media screen and (max-width: 767px) {
    content {
      display: none;
    }
    mcontent {
      display: inherit;
    }

    .offices {
      padding-top: 50px;
      background: #efefef;
    }
    .offices-content {
      padding: 20px 0px;
      width: 100%;
    }
  }
`;
