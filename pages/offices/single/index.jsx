import { Component } from "react";
import Page from "../../../layouts/Main";
import styles from "./styles";
import TopBar from "../../../components/offices/single/TopBar";
import MainContent from "../../../components/offices/single/MainContent";
import ListContent from "../../../components/offices/single/ListContent";
import AdditionalContent from "../../../components/offices/single/AdditionalContent";
import PropTypes from "prop-types";
import ContactAgentForm from "../../../components/shared/ContactAgentForm";

class OfficePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isContactAgent: false
    };

    this.contactAgentAction = this.contactAgentAction.bind(this);
  }

  contactAgentAction = () => {
    const { isContactAgent } = this.state;

    this.setState({
      isContactAgent: !isContactAgent
    });
  };

  render() {
    const { office } = this.props;
    const { isContactAgent } = this.state;

    return (
      <Page>
        <content>
          <TopBar
            office={office}
            contactAgentAction={this.contactAgentAction}
            isContactAgent={isContactAgent}
          />
          {isContactAgent ? (
            <ContactAgentForm />
          ) : (
            <div className="office-content">
              <div className="detail-info row">
                <div className="main-info col-sm-8">
                  <MainContent office={office} />
                </div>
                <div className="additional-info col-sm-4">
                  <AdditionalContent office={office} />
                </div>
              </div>
              <div className="list-info row">
                <div className="col-md-8">
                  <ListContent />
                </div>
              </div>
            </div>
          )}
        </content>
        <mcontent>
          <TopBar
            office={office}
            contactAgentAction={this.contactAgentAction}
            isContactAgent={isContactAgent}
          />
          {isContactAgent ? (
            <ContactAgentForm />
          ) : (
            <div className="office-content">
              <MainContent office={office} />
              <AdditionalContent office={office} />
              <ListContent />
            </div>
          )}
        </mcontent>
        <style jsx>{styles}</style>
      </Page>
    );
  }
}

OfficePage.defaultProps = {
  office: {
    title: "Our Elwood Office",
    address: "90 Ormond Road, Elwood Vic 3184",
    email: "info@chisholmgamon.com.au",
    phone: "03 9531 1245",
    fax: "03 9531 1245",
    openingHours: [
      { weekday: "Monday", hours: "8:45am-5:30pm" },
      { weekday: "Tuesday", hours: "8:45am-5:30pm" },
      { weekday: "Wednesday", hours: "8:45am-5:30pm" },
      { weekday: "Thursday", hours: "8:45am-5:30pm" },
      { weekday: "Friday", hours: "8:45am-5pm" },
      { weekday: "Saturday", hours: "9:00am-1pm" },
      { weekday: "Sunday", hours: "Closed" }
    ]
  }
};

OfficePage.propTypes = {
  office: PropTypes.shape({
    title: PropTypes.string.isRequired,
    address: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    phone: PropTypes.string.isRequired,
    fax: PropTypes.string.isRequired,
    openingHours: PropTypes.arrayOf(
      PropTypes.shape({
        weekday: PropTypes.string.isRequired,
        hours: PropTypes.string.isRequired
      })
    ).isRequired
  })
};

export default OfficePage;
