import css from "styled-jsx/css";

export default css`
  content {
    display: inherit;
  }
  mcontent {
    display: none;
  }
  .detail-info,
  .list-info {
    background: #efefef;
    margin: 0px;
  }
  .additional-info {
    background: #efefef;
  }
  @media screen and (max-width: 767px) {
    content {
      display: none;
    }
    mcontent {
      background: #efefef;
      display: inherit;
    }
  }
`;
