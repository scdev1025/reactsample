import Page from "../../layouts/Main";
import styles from "./styles";
import Pagenation from "../../components/shared/Pagenation";
import BlogCarousel from "../../components/blog/BlogCarousel";
import BlogList from "../../components/blog/BlogList";
import SignUpForm from "../../components/blog/SignUpForm";
import SearchContainer from "../../components/blog/SearchContainer";
import PropTypes from "prop-types";

const BlogPage = props => (
  <Page>
    <content>
      <BlogCarousel blogList={props.blogList} />
      <div className="blog-list-container flex-column-center">
        <div className="blog-header container-fluid">
          <div className="heading text-center orange">BLOG</div>
          <SearchContainer />
        </div>
        <div className="blog-list container-fluid">
          <div className="row">
            <div className="col-md-8 list">
              <BlogList blogList={props.blogList} />
              <Pagenation pageCount={7} currentPage={1} />
            </div>
            <div className="col-md-4 signup-form">
              <SignUpForm />
            </div>
          </div>
        </div>
      </div>
    </content>
    <mcontent>
      <BlogCarousel blogList={props.blogList} />
      <div className="blog-header container-fluid">
        <div className="heading text-center orange">BLOG</div>
      </div>
      <SearchContainer />
      <BlogList blogList={props.blogList} />
      <Pagenation pageCount={7} currentPage={1} />
      <SignUpForm />
    </mcontent>
    <style jsx>{styles}</style>
  </Page>
);

BlogPage.defaultProps = {
  blogList: [
    {
      title: "Nam porttitor blandit accumsan.",
      content:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et atiquam erat laoreet. Sed sit amet arcu aliquet.",
      category: "Category",
      date: "July 18, 2018",
      imageURL: "../../static/images/offices/img_office_blackrock.png"
    },
    {
      title: "Nam porttitor blandit accumsan.",
      content:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et atiquam erat laoreet. Sed sit amet arcu aliquet.",
      category: "Category",
      date: "July 18, 2018",
      imageURL: "../../static/images/offices/img_office_elwood.png"
    },
    {
      title: "Nam porttitor blandit accumsan.",
      content:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et atiquam erat laoreet. Sed sit amet arcu aliquet.",
      category: "Category",
      date: "July 18, 2018",
      imageURL: "../../static/images/offices/img_office_portmelboume.png"
    }
  ]
};

BlogPage.propTypes = {
  blogList: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      content: PropTypes.string.isRequired,
      category: PropTypes.string.isRequired,
      date: PropTypes.string.isRequired,
      imageURL: PropTypes.string.isRequired
    })
  ).isRequired
};

export default BlogPage;
