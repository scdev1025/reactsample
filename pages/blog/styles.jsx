import css from "styled-jsx/css";

export default css`
  content {
    display: inherit;
  }
  mcontent {
    display: none;
  }

  content {
    padding-top: 90px;
    background: #efefef;
  }

  .blog-header {
    padding: 30px;
    padding-bottom: 10px;
  }

  .blog-header .heading {
    font-size: 2rem;
    font-weight: 500;
  }

  .signup-form {
    padding: 0px;
  }

  @media screen and (max-width: 767px) {
    content {
      display: none;
    }
    mcontent {
      display: inherit;
    }
  }
`;
