import css from "styled-jsx/css";

export default css`
  content {
    background: #efefef;
    display: inherit;
  }
  mcontent {
    display: none;
  }

  .mid-content {
    background: #fff;
    padding-top: 50px;
  }

  .profile-img {
    max-width: 202px;
    max-height: 202px;
    border-radius: 100%;
    border: #979797 1px solid;
    margin: 0 auto;
  }

  .get-touch {
    margin-bottom: 20px;
  }

  .get-touch .title {
    margin-bottom: 30px;
  }

  .testimonial {
    margin: 30px 0px;
  }

  .listing {
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: flex-start;
  }

  .see-all .btn {
    -webkit-box-shadow: 2px 2px 5px 2px rgba(0, 0, 0, 0.21);
    -moz-box-shadow: 2px 2px 5px 2px rgba(0, 0, 0, 0.21);
    float: right;
    box-shadow: 2px 2px 5px 2px rgba(0, 0, 0, 0.21);
    background: #fff;
    color: #221e1f !important;
    font-size: 1.5rem;
    max-width: 177px !important;
    min-width: 177px !important;
    border-radius: 0;
    margin-right: 10px;
  }

  .block-heading {
    background: #ff8124;
    margin-left: -15px;
    margin-right: -15px;
    padding: 0.625rem 1.75rem;
    margin-bottom: 30px;
  }

  .block-heading h3 {
    font-size: 2.1rem;
    color: #fff;
    margin-top: 10px;
  }

  @media screen and (max-width: 767px) {
    content {
      display: none;
    }
    mcontent {
      display: inherit;
    }

    .profile-img {
      max-width: 202px;
      max-height: 202px;
      margin-bottom: 30px;
    }

    .testimonial {
      background: #efefef;
      margin: 0px -15px;
      padding: 10px 0px;
      height: 350px;
    }

    .get-touch .title {
      margin: 20px 0px;
    }
  }
`;
