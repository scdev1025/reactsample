import Page from "../../layouts/Main";
import { Component } from "react";
import styles from "./styles";
import Testimonial from "../../components/shared/Testimonial";
import LatestListing from "../../components/staff/LatestListing";
import TopBar from "../../components/staff/TopBar";
import GetInTouchForm from "../../components/shared/GetInTouchForm";
import MobileGetInTouchForm from "../../components/shared/MobileGetInTouchForm";
import ReadMoreButton from "../../components/shared/ReadMoreButton";
import ContactAgentForm from "../../components/shared/ContactAgentForm";
import PropTypes from "prop-types";

const imgProfile = "../../static/images/img_profile.png";

class StaffPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isContactAgent: false
    };

    this.contactAgentAction = this.contactAgentAction.bind(this);
  }

  contactAgentAction = () => {
    const { isContactAgent } = this.state;

    this.setState({
      isContactAgent: !isContactAgent
    });
  };

  render() {
    const { agent } = this.props;
    const { isContactAgent } = this.state;

    return (
      <Page>
        <content>
          <TopBar
            agent={agent}
            contactAgentAction={this.contactAgentAction}
            isContactAgent={isContactAgent}
          />
          {isContactAgent ? (
            <ContactAgentForm />
          ) : (
            [
              <div className="mid-content">
                <div className="container-fluid">
                  <div className="row">
                    <div className="col-md-8">
                      <div className="row">
                        <div className="col-md-4">
                          <div className="profile-img">
                            <img src={imgProfile} alt="profile" />
                          </div>
                        </div>
                        <div className="col-md-8 min-small-size">
                          <p>
                            As Managing Director of Bayside's leading real
                            estate team, Torsten is committed to setting the
                            highest standards of excellence to which the whole
                            team aspires.{" "}
                          </p>
                          <p>
                            His hands on involvement with the business means he
                            is continually using his experience, extensive
                            industry knowledge and dedication to inspire and
                            motivate colleagues while delivering exceptional
                            results for his clients.{" "}
                          </p>
                          <p>
                            Every day Torsten brings his marketing, negotiation
                            and communication skills to the benefit of each and
                            every client transaction.{" "}
                          </p>
                          <p>
                            "We take our responsibility to our clients very
                            seriously". says Torsten. "This means ensuring we
                            are informed and up to date with the latest market
                            news and trends. It also means we try to set new
                            benchmarks in professionalism, ethical behavior and
                            final outcomes".{" "}
                          </p>
                          <p>
                            Torsten's education and qualifications put him at
                            the forefront of the industry as a licensed
                            auctioneer, agent and industry speaker.{" "}
                          </p>
                          <p>
                            In his spare time Torsten enjoys watching the
                            football, playing tennis, swimming at the local pool
                            and spending quality time with his family and
                            friends.{" "}
                          </p>
                          <p>
                            Chisholm & Gamon Property delivers premium results
                            throughout Bayside and beyond. With offices in
                            Elwood, Port Melbourne, Black Rock and Mt Martha, we
                            focus on customer satisfaction and personal
                            integrity.
                          </p>
                          <p>
                            Torsten Kasper is a dynamic leader of this team.
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-4 get-touch">
                      <div className="title orange large-max-size medium-font-weight">
                        Get in touch
                      </div>
                      <GetInTouchForm />
                    </div>
                  </div>
                </div>
              </div>,
              <div className="col-md-8 testimonial">
                <Testimonial />
              </div>,
              <div className="listing">
                <div className="col-md-8 p-0 latest-sale-listing">
                  <LatestListing title="LATEST LISTING FOR SALE" />
                </div>
                <div className="col-md-8 p-0 latest-sold-listing">
                  <LatestListing title="LATEST SOLD LISTINGS" />
                </div>
              </div>
            ]
          )}
        </content>
        <mcontent>
          <TopBar
            agent={agent}
            contactAgentAction={this.contactAgentAction}
            isContactAgent={isContactAgent}
          />
          {isContactAgent ? (
            <ContactAgentForm />
          ) : (
            [
              <div className="mid-content">
                <div className="container-fluid">
                  <div className="row">
                    <div className="col-md-8">
                      <div className="row">
                        <div className="col-md-4">
                          <div className="profile-img">
                            <img src={imgProfile} alt="profile" />
                          </div>
                        </div>
                        <div className="col-md-8 min-small-size">
                          <p>
                            As Managing Director of Bayside's leading real
                            estate team, Torsten is committed to setting the
                            highest standards of excellence to which the whole
                            team aspires.{" "}
                          </p>
                          <p>
                            His hands on involvement with the business means he
                            is continually using his experience, extensive
                            industry knowledge and dedication to inspire and
                            motivate colleagues while delivering exceptional
                            results for his clients.{" "}
                          </p>
                          <ReadMoreButton />
                        </div>
                      </div>
                      <div className="testimonial">
                        <Testimonial />
                      </div>
                    </div>
                    <div className="col-md-4 get-touch">
                      <div className="title orange large-max-size medium-font-weight">
                        Get in touch
                      </div>
                      <MobileGetInTouchForm />
                    </div>
                  </div>
                </div>
              </div>,
              <div className="listing">
                <div className="latest-sale-listing">
                  <LatestListing title="LATEST LISTING FOR SALE" />
                </div>
                <div className="latest-sold-listing">
                  <LatestListing title="LATEST SOLD LISTINGS" />
                </div>
              </div>
            ]
          )}
        </mcontent>
        <style jsx>{styles}</style>
      </Page>
    );
  }
}

StaffPage.defaultProps = {
  agent: {
    firstName: "Torsten",
    lastName: "Kasper",
    title: "Managing Director",
    phone: "0428 454 181",
    fax: "03 9531 1245"
  }
};

StaffPage.propTypes = {
  agent: PropTypes.shape({
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    phone: PropTypes.string.isRequired,
    fax: PropTypes.string.isRequired
  }).isRequired
};

export default StaffPage;
