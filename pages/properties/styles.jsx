import css from "styled-jsx/css";

export default css`
  content {
    display: inherit;
  }
  mcontent {
    display: none;
  }
  content {
    padding-top: 100px;
  }
  .notifyme-container { 
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;    
    background: rgba(0,0,0,0.9);
    z-index: 10;
    padding-top: 200px;
  }

-webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
  }
  .properties-list-container {
    background: #efefef;
  }
  .properties-list-container .heading {
    margin-top: 50px;
  }
  .properties-list-container .properties-list {
    margin-top: 30px;
    flex: 1;
  }
  .properties-list-container .properties-list .list {
    padding-left: 30px;
  }
  .properties-list-container .properties-list .map {
    width: 100%;
  }
  .properties-list-container .properties-list .map .img-background {
    height: 1000px;
    padding: 20px;
  }
  .properties-list-container
    .properties-list
    .map
    .img-background
    .btn.draw-search {
    background: #221e1f;
    float: right;
  }
  .properties-list-container .properties-list .map .map-item {    
    position: relative;
    diplay: inline-block;    
    transform: translate(40vw, 30vh);
  }

  .properties-list button {
    background: #ff8124;
    border: none;
    border-radius: 0;
    padding: 5px 20px;
  }

  @media screen and (max-width: 767px) {
    content {
      display: none;
    }
    mcontent {
      display: inherit;
    }
    .notifyme-container { 
      padding-top: 100px;
    }
    .properties-list {
      padding-bottom: 20px;
      background: #efefef;
    }
    .properties-list .heading-container {
      padding: 30px 10px;
    }
    .properties-list button {
      width: 90%;
      margin-top: 20px;
    }
    .properties-list .map .img-background{
      height: 600px;
      margin-bottom: -20px;
    }
  }
`;
