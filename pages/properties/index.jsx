import { Component } from "react";
import Page from "../../layouts/Main";
import styles from "./styles";
import PerfectPlace from "../../components/properties/PerfectPlace";
import MobilePerfectPlace from "../../components/properties/MobilePerfectPlace";
import Pagenation from "../../components/shared/Pagenation";
import NewsLetter from "../../components/home/NewsLetter";
import MobileNewsLetter from "../../components/home/MobileNewsLetter";
import ListingItem from "../../components/shared/ListingItem";
import PropertyMapItem from "../../components/shared/PropertyMapItem";
import PropTypes from "prop-types";
import NotifyMe from "../../components/properties/NotifyMe";

const imgMapProperties =
  "../../static/images/properties/img_map_properties.png";

class PropertiesPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isExpand: false,
      isNotify: false
    };

    this.onPressExpand = this.onPressExpand.bind(this);
    this.onPressNotifyMe = this.onPressNotifyMe.bind(this);
  }

  onPressExpand() {
    const { isExpand } = this.state;
    this.setState({
      isExpand: !isExpand
    });
  }

  onPressNotifyMe(notifyValue) {
    this.setState({
      isNotify: notifyValue
    });
  }

  render() {
    const { count, address } = this.props;
    const { isExpand, isNotify } = this.state;

    return (
      <Page>
        <content>
          {isNotify ? (
            <div className="notifyme-container">
              <NotifyMe cancelAction={this.onPressNotifyMe} />
            </div>
          ) : null}
          <PerfectPlace notifyAction={this.onPressNotifyMe} />
          <div className="properties-list-container flex-column-center">
            <div className="heading orange small-medium-size medium-font-weight">
              {count} PROPERTIES TO BUY
            </div>
            <div className="sub-heading black min-small-size medium-font-weight">
              {address}
            </div>
            <div className="properties-list container-fluid">
              {isExpand ? (
                <div className="row">
                  <div className="map">
                    <div
                      className="img-background"
                      style={{
                        backgroundImage: `url(${imgMapProperties})`
                      }}
                    >
                      <button
                        type="button"
                        className="btn btn-primary min-small-size shadow"
                        onClick={() => this.onPressExpand()}
                      >
                        Show list
                      </button>
                      <button
                        type="button"
                        className="btn btn-primary draw-search min-small-size shadow"
                      >
                        Draw search on map
                      </button>
                      <div className="map-item">
                        <PropertyMapItem />
                      </div>
                    </div>
                  </div>
                </div>
              ) : (
                <div className="row">
                  <div className="col-md-8 list">
                    <ListingItem />
                    <ListingItem />
                    <ListingItem />
                    <ListingItem />
                    <ListingItem />
                    <ListingItem />
                    <ListingItem />
                    <ListingItem />
                    <Pagenation pageCount={7} currentPage={1} />
                  </div>
                  <div className="col-md-4 map">
                    <div
                      className="img-background"
                      style={{
                        backgroundImage: `url(${imgMapProperties})`
                      }}
                    >
                      <button
                        type="button"
                        className="btn btn-primary min-small-size shadow"
                        onClick={() => this.onPressExpand()}
                      >
                        Expand map
                      </button>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
          <NewsLetter />
        </content>
        <mcontent>
          {isNotify ? (
            <div className="notifyme-container">
              <NotifyMe cancelAction={this.onPressNotifyMe} />
            </div>
          ) : null}
          <MobilePerfectPlace notifyAction={this.onPressNotifyMe} />
          <div className="properties-list">
            <div className="heading-container flex-column-center">
              <div className="heading orange medium-size medium-font-weight text-center">
                {count} PROPERTIES TO BUY
              </div>
              <div className="sub-heading black min-small-size medium-font-weight text-center">
                {address}
              </div>
              <button
                type="button"
                className="btn btn-primary min-small-size shadow"
                onClick={() => this.onPressExpand()}
              >
                {isExpand ? "Show list" : "Show Map"}
              </button>
            </div>
            {isExpand ? (
              <div className="map">
                <div
                  className="img-background"
                  style={{ backgroundImage: `url(${imgMapProperties})` }}
                />
              </div>
            ) : (
              <div className="list">
                <ListingItem />
                <ListingItem />
                <ListingItem />
                <ListingItem />
                <Pagenation pageCount={7} currentPage={4} />
              </div>
            )}
          </div>
          <MobileNewsLetter />
        </mcontent>
        <style jsx>{styles}</style>
      </Page>
    );
  }
}

PropertiesPage.defaultProps = {
  count: 54,
  address: "in Elwood, Elsternwick and Port Melbourne"
};

PropertiesPage.propTypes = {
  count: PropTypes.number.isRequired,
  address: PropTypes.string.isRequired
};

export default PropertiesPage;
