import { Component } from "react";
import Page from "../../../layouts/Main";
import styles from "./styles";
import NewsLetter from "../../../components/home/NewsLetter";
import MobileNewsLetter from "../../../components/home/MobileNewsLetter";
import TopBar from "../../../components/properties/single/TopBar";
import ImageContainer from "../../../components/properties/single/ImageContainer";
import MainContent from "../../../components/properties/single/MainContent";
import AdditionalContent from "../../../components/properties/single/AdditionalContent";
import ContactAgentForm from "../../../components/shared/ContactAgentForm";
import PropTypes from "prop-types";

class PropertyPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isContactAgent: false
    };

    this.contactAgentAction = this.contactAgentAction.bind(this);
  }

  contactAgentAction = () => {
    const { isContactAgent } = this.state;

    this.setState({
      isContactAgent: !isContactAgent
    });
  };

  render() {
    const { property } = this.props;
    const { isContactAgent } = this.state;

    return (
      <Page>
        <content>
          <TopBar
            property={property}
            contactAgentAction={this.contactAgentAction}
            isContactAgent={isContactAgent}
          />
          {isContactAgent ? (            
            <ContactAgentForm />
          ) : (
            [
              <div className="property-content">
                <div className="image-info">
                  <ImageContainer imageList={property.imageList} />
                </div>
                <div className="detail-info row">
                  <div className="main-info col-md-8">
                    <MainContent property={property} />
                  </div>
                  <div className="additional-info col-md-4">
                    <AdditionalContent property={property} />
                  </div>
                </div>
              </div>,
              <NewsLetter />
            ]
          )}
        </content>
        <mcontent>
          <div className="property-content">
            <TopBar
              property={property}
              contactAgentAction={this.contactAgentAction}
              isContactAgent={isContactAgent}
            />
            {isContactAgent ? (
              <ContactAgentForm />
            ) : (
              [
                <ImageContainer imageList={property.imageList} />,
                <MainContent property={property} />,
                <AdditionalContent property={property} />
              ]
            )}
          </div>
          {isContactAgent ? null : <MobileNewsLetter />}
        </mcontent>
        <style jsx>{styles}</style>
      </Page>
    );
  }
}

PropertyPage.defaultProps = {
  property: {
    title: "St Kilda",
    address: "23 Baker Street",
    subtitle: "A HOME WITH TRUE HEART, SPACE AND SOUL",
    bed_count: 4,
    bath_count: 2,
    car_count: 1,
    agent: {
      firstName: "Torsten",
      lastName: "Kasper",
      profileImageURL: "../../static/images/img_list_profile.png"
    },
    featureList: [
      "Something",
      "Another thing",
      "Something else",
      "Insert great feature"
    ],
    price: "Offers over $1,000,000",
    auction: "Sat 8 August @ 10am",
    inspectionList: [
      "Sat 8 August @ 10am - 10:30am",
      "Sun 9 August @ 9:15am - 9:30am",
      "Wed 12 August @ 4:15pm - 4:45pm"
    ],
    imageList: [
      "../../static/images/home/img_property_sales.png",
      "../../static/images/home/img_property_rental.png",
      "../../static/images/home/img_property_sales.png",
      "../../static/images/home/img_property_rental.png"
    ]
  }
};

PropertyPage.propTypes = {
  property: PropTypes.shape({
    title: PropTypes.string.isRequired,
    address: PropTypes.string.isRequired,
    subtitle: PropTypes.string.isRequired,
    bed_count: PropTypes.number.isRequired,
    bath_count: PropTypes.number.isRequired,
    car_count: PropTypes.number.isRequired,
    agent: PropTypes.shape({
      firstName: PropTypes.string.isRequired,
      lastName: PropTypes.string.isRequired,
      profileImageURL: PropTypes.string.isRequired
    }),
    featureList: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
    price: PropTypes.string.isRequired,
    auction: PropTypes.string.isRequired,
    inspectionList: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
    imageList: PropTypes.arrayOf(PropTypes.string).isRequired
  }).isRequired
};

export default PropertyPage;
