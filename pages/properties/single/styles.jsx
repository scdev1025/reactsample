import css from "styled-jsx/css";

export default css`
  content {
    display: inherit;
  }
  mcontent {
    display: none;
  }
  .detail-info {
    background: white;
    margin: 0px;
  }
  .additional-info {
    background: #efefef;
  }

  @media screen and (max-width: 767px) {
    content {
      display: none;
    }
    mcontent {
      display: inherit;
    }
  }
`;
