import Page from "../layouts/Main";
import styles from "./styles";
import Testimonial from "../components/shared/Testimonial";
import ActionBox from "../components/home/ActionBox";
import MobileActionBox from "../components/home/MobileActionBox";
import FeaturedListings from "../components/home/FeaturedListings";
import MobileFeaturedListings from "../components/home/MobileFeaturedListings";
import NewsLetter from "../components/home/NewsLetter";
import MobileNewsLetter from "../components/home/MobileNewsLetter";
import FastBlock from "../components/home/FastBlock";
import MobileFastBlock from "../components/home/MobileFastBlock";
import LatestArticles from "../components/home/LatestArticles";
import MobileLatestArticles from "../components/home/MobileLatestArticles";
import MobileSearchBar from "../components/home/MobileSearchBar";
import ImageBlock from "../components/home/ImageBlock";
import MobileImageBlock from "../components/home/MobileImageBlock";
import PropTypes from "prop-types";
import {
  whiteLogo,
  whiteLogo_2x,
  houseIcon,
  personIcon,
  markerIcon
} from "../components/shared/variables";

const communityBg = "../static/images/home/img_community_bg.png";

const Index = props => (
  <Page>
    <content>
      <intro>
        <ActionBox />
        <div className="intro-center flex-column-center">
          <div className="arrow-down-container flex-column-center shadow">
            <a href="#">
              <i className="fa fa-arrow-down white small-size" />
            </a>
          </div>
        </div>
        <FeaturedListings
          salesProperty={props.salesProperty}
          rentalProperty={props.rentalProperty}
        />
        <NewsLetter />
      </intro>
      <choose className="flex-column-center">
        <div className="orange title">
          <p>WHY CHOOSE US?</p>
        </div>
        <div className="black-text">
          <p>It's more about people than it is about property</p>
        </div>
        <ImageBlock
          iconImage={markerIcon}
          bgImage={props.locations.imageURL}
          title={props.locations.count + " OFFICES"}
          list={props.locations.list}
          imagePosition={"right"}
          isOffice={true}
          link="/offices"
        />
        <ImageBlock
          iconImage={personIcon}
          bgImage={props.people.imageURL}
          title={props.people.count + " PEOPLE"}
          list={props.people.list}
          imagePosition={"left"}
          isOffice={false}
          link="/staff"
        />
        <ImageBlock
          iconImage={houseIcon}
          bgImage={props.properties.imageURL}
          title={props.properties.count + " PROPERTIES"}
          list={props.properties.list}
          imagePosition={"right"}
          isOffice={false}
          link="/properties"
        />
        <div>
          <button type="button" className="btn btn-primary small-size">
            Discover our difference
          </button>
        </div>
      </choose>
      <articles className="row no-gutters">
        <FastBlock fastList={props.fastList} />
        <LatestArticles articleList={props.latestArticles} />
      </articles>
      <testimonial className="row no-gutters">
        <div className="col-md-8 testimonial-slider flex-column-center">
          <Testimonial />
        </div>
        <div className="col-md-4 testimonial-community flex-column-center">
          <div
            className="img-background blur-image"
            style={{ backgroundImage: `url(${communityBg})` }}
          />
          <img src={whiteLogo_2x} alt="img_logo_white" />
          <div className="white title text-center">IN THE COMMUNITY</div>
          <button
            type="button"
            className="btn btn-primary normal-btn small-size shadow"
          >
            Learn with us
          </button>
        </div>
      </testimonial>
    </content>

    <mcontent>
      <intro>
        <MobileActionBox />
        <div className="intro-center flex-column-center">
          <div className="hello-container flex-column-center shadow">
            <div className="white min-small-size medium-font-weight">hello</div>
          </div>
        </div>
        <MobileSearchBar />
        <MobileFeaturedListings
          salesProperty={props.salesProperty}
          rentalProperty={props.rentalProperty}
        />
        <MobileNewsLetter />
      </intro>
      <choose className="flex-column-center">
        <div className="orange small-medium-size medium-font-weight text-center">
          <p>WHY CHOOSE US?</p>
        </div>
        <div className="black small-size text-center">
          <p>It's more about people than it is about property</p>
        </div>
        <div className="container-fluid flex-column-center">
          <MobileImageBlock
            iconImage={markerIcon}
            title={props.locations.count + " OFFICES"}
            link="/offices"
          />
          <MobileImageBlock
            iconImage={personIcon}
            title={props.people.count + " PEOPLE"}
            link="/staff"
          />
          <MobileImageBlock
            iconImage={houseIcon}
            title={props.properties.count + " PROPERTIES"}
            link="/properties"
          />
        </div>
        <div>
          <button
            type="button"
            className="btn btn-primary normal-btn min-small-size shadow"
          >
            Discover our difference
          </button>
        </div>
      </choose>
      <articles className="row no-gutters">
        <MobileFastBlock fastList={props.fastList} />
        <MobileLatestArticles articleList={props.latestArticles} />
      </articles>
      <testimonial className="row no-gutters">
        <div className="col-md-12 testimonial-slider flex-column-center">
          <Testimonial />
        </div>
        <div className="col-md-12 testimonial-community flex-column-center">
          <div
            className="img-background blur-image"
            style={{ backgroundImage: `url(${communityBg})` }}
          />
          <img src={whiteLogo} alt="img_logo_white" />
          <div className="white medium-size text-center">IN THE COMMUNITY</div>
          <button
            type="button"
            className="btn btn-primary normal-btn min-small-size shadow"
          >
            Learn with us
          </button>
        </div>
      </testimonial>
    </mcontent>
    <style jsx>{styles}</style>
  </Page>
);

Index.defaultProps = {
  salesProperty: {
    title: "St Kilda",
    imageURL: "../static/images/home/img_property_sales.png",
    bed_count: 4,
    bath_count: 2,
    car_count: 1
  },
  rentalProperty: {
    title: "Brighton East",
    imageURL: "../static/images/home/img_property_rental.png",
    bed_count: 4,
    bath_count: 2,
    car_count: 1
  },
  latestArticles: [
    {
      title: "C&G's guide to the Good Food & Wine Show Melbourne",
      content:
        "The Good Food & Wine Show is back and better than ever before, tempting you with another...",
      author: "Chisholm & Gamon",
      date: "24 May 2018"
    },
    {
      title: "C&G's guide to the Good Food & Wine Show Melbourne",
      content:
        "The Good Food & Wine Show is back and better than ever before, tempting you with another...",
      author: "Chisholm & Gamon",
      date: "24 May 2018"
    }
  ],
  fastList: [
    {
      title: "What's my home worth?",
      content: "Don't settle for less, get a free appraisal"
    },
    {
      title: "Time for something new",
      content: "Buy a brand new property off the plan"
    },
    {
      title: "Exclusive properties",
      content: "Shh.. The special ones ONLY on our website"
    },
    {
      title: "I'm here for business",
      content: "Commercial properties and advice"
    },
    {
      title: "Bayside living",
      content: "Learn local knowledge with our suburb profiles"
    },
    {
      title: "Work with us",
      content: "We're always looking for amazing people"
    }
  ],
  locations: {
    count: 4,
    imageURL: "../static/images/home/img_choose_office.png",
    list: [
      { office: "Elwood", address: "90 Ormond Road" },
      { office: "Port Melboume", address: "1/103D Bay Street, Port Melboume" },
      { office: "Black Rock", address: "3 Bluff Road, Black Rock" },
      { office: "Mount Martha", address: "7A Bay Road, Mount Martha" }
    ]
  },
  people: {
    count: 50,
    imageURL: "../static/images/home/img_choose_people.png",
    list: [
      "Manangement team",
      "Sales team",
      "Property Management team",
      "Commercial team",
      "Administration team"
    ]
  },
  properties: {
    count: 85,
    imageURL: "../static/images/home/img_choose_property.png",
    list: [
      "Properties for sale",
      "Properties for rent",
      "Commercial properties",
      "Sold properties",
      "Leased properties"
    ]
  }
};

Index.propTypes = {
  salesProperty: PropTypes.shape({
    title: PropTypes.string.isRequired,
    imageURL: PropTypes.string.isRequired,
    bed_count: PropTypes.number.isRequired,
    bath_count: PropTypes.number.isRequired,
    car_count: PropTypes.number.isRequired
  }).isRequired,
  rentalProperty: PropTypes.shape({
    title: PropTypes.string.isRequired,
    imageURL: PropTypes.string.isRequired,
    bed_count: PropTypes.number.isRequired,
    bath_count: PropTypes.number.isRequired,
    car_count: PropTypes.number.isRequired
  }).isRequired,
  latestArticles: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      address: PropTypes.string.isRequired,
      phone: PropTypes.string.isRequired,
      image: PropTypes.string.isRequired
    })
  ).isRequired,
  fastList: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      content: PropTypes.string.isRequired
    })
  ).isRequired,
  locations: PropTypes.shape({
    count: PropTypes.number.isRequired,
    imageURL: PropTypes.string.isRequired,
    list: PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string.isRequired,
        content: PropTypes.string.isRequired
      })
    ).isRequired
  }).isRequired,
  people: PropTypes.shape({
    count: PropTypes.number.isRequired,
    imageURL: PropTypes.string.isRequired,
    list: PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string.isRequired,
        content: PropTypes.string.isRequired
      })
    ).isRequired
  }).isRequired,
  properties: PropTypes.shape({
    count: PropTypes.number.isRequired,
    imageURL: PropTypes.string.isRequired,
    list: PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string.isRequired,
        content: PropTypes.string.isRequired
      })
    ).isRequired
  }).isRequired
};

export default Index;
