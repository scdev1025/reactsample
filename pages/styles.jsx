import css from "styled-jsx/css";

export default css`
  content {
    display: inherit;
  }
  mcontent {
    display: none;
  }

  .intro-center {
    position: absolute;
    transform: translateY(-50%);
    width: 100%;
    z-index: 1;
  }
  .arrow-down-container {
    background-color: #ff8124;
    width: 45px;
    height: 45px;
    border-radius: 100%;
    text-align: center;
  }
  .arrow-down-container a i {
    text-decoration: none !important;
  }
  choose {
    background: #f3f3f3;
    height: 1600px;
    padding: 50px;
  }
  .list li {
    margin-bottom: 20px;
  }

  articles {
    height: 700px;
  }

  testimonial {
    height: 300px;
  }
  .testimonial-slider {
    background-color: #f3f3f3;
  }
  testimonial .title {
    margin-top: 20px;
  }
  .testimonial-community .normal-btn {
    display: none;
  }

  .btn {
    border: none;
    border-radius: 0;
    background: #ff8124;
    color: #fff;
    padding: 15px 20px;
  }
  .normal-btn {
    padding: 10px 10px;
  }

  @media screen and (max-width: 767px) {
    content {
      display: none;
    }
    mcontent {
      display: inherit;
    }
    .hello-container {
      background-color: #ff8124;
      width: 70px;
      height: 70px;
      border-radius: 100%;
      text-align: center;
    }
    choose {
      background: #f3f3f3;
      height: 650px;
      padding: 30px;
    }
    articles {
      height: 900px;
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
    }

    testimonial {
      height: 600px;
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
    }
    .testimonial-slider {
      background-color: #f3f3f3;
      flex: 1;
    }
    .testimonial-community {
      flex: 1;
    }
    .testimonial-community .normal-btn {
      display: block;
    }
    .testimonial-community .text-center {
      margin: 1.5rem 0;
    }
  }
`;
